## Description

Dataset annotator is tool, which helps define multiple data source modules, which obtain information about list of IP addresses (or list of network prefixes in CIRD notation or hostnames --> they are translated into IP addreses). Gathered informations are saved in **.json** file and can be used as annotation of captured traffic.

Currently is supported only annotation of IPv4 addresses.



## Project structure

`configuration` - contains all needed configuration of annotator

- **configuration.yml** - whole configuration of annotator
  - the most important is **input** configuration specifying path to input identifiers and their type
  - **enrichment** defines all used data sources (*enabled_data_sources*) and integrators (*enabled_integrators*) and configuration of these

`data` - place for static data of some modules

Inside `src` folder there are all source files including basic tests.

- **common/** - all common functionality for whole dataset annotator, mainly Configuration singleton class for globally sharing loaded configuration and then some utility functions.
- **enrichment/** - all source code, which somehow enriches final annotation structure of certain IP address
  - **processing_chains/** - defines *enrichment chains*, which do all handle processing of annotation of IP address. While annotating IP address data, there is shared some common functionality between modules (e.g. multiple data sources, which obtains data about IP address). Then it is easy to group these modules into chain and run these modules one by one. That is the purpose of enrichment chain (e.g. run all data sources is handled by data_sources_chain). Main advantages of this design:
    - all modules are called from one specific place (**enrichment/processing_chains/runner.py**), which runs all chains (and their modules)
    - it is easier to add new module to the chain, for example to introduce new data source for annotation
  - **data_sources/** - all modules handling downloading data from annotator's data sources, which are used for annotation
  - **integrators/** - these modules are used to integrate data from multiple data sources into one in final annotation structure
- **input_processing** - parsing of input files, which contains input identifiers (IP addresses, IP networks, ...), and prepares it for enrichment
- **tests/** - includes some basic tests of integrators using *pytest* Python package, because their functionality is crucial for annotation
  - data sources are important too, but their output differs in time, so it is hard to create static tests for these and they usually do not contain some hard processing
  - tests should not be generally placed inside **src/** folder, but I was not able to run these test outside of **src/** folder because of import errors

`standalone_scripts` - contains scripts, which are not directly used by annotator, but are useful in process of creating new dataset (mainly scripts for handling captured data by Nemea)



## Prerequisites

1. Install all dependencies
   - ideally create virtual environment by `$ python3 -m venv venv`, activate it by `$ source venv/bin/activate` and install all required packages by `$ pip install -r requirements.txt`
2. Update **whatportis**'s IANA port database by running `$ whatportis --update` in the console.



## Run

### Dataset annotator

1. Go through all prerequisites defined above
2. Create list of input identifiers (e.g. list of IP addresses, which you want to annotate)
3. Configure annotator in **configuration/configuration.yml**
4. Run annotator (with activated virtual environment) 

But first it is **recommended to run the annotator on just one IP** address, to check annotators output, if final annotation structure is correct based on configuration.

### Tests

Fill Shodan's api key in **src/tests/integrators/conf.yml** and run `$ pytest` inside **/src/tests**





