Additional notes that provide an overview of the attributes of the **basic flow data**:

| Název atributu | Popis atributu                          |
| -------------- | --------------------------------------- |
| DST_IP         | Destination IP address                  |
| SRC_IP         | Source IP address                       |
| DST_PORT       | Destination port number of flow         |
| SRC_PORT       | Source port number of flow              |
| BYTES          | Count of transferred bytes              |
| PACKETS        | Count of transferred packets            |
| TIME_FIRST     | Date and time when a flow started       |
| TIME_LAST      | Date and time when a flow ended         |
| LINK_BIT_FIELD | line identification                     |
| DIR_BIT_FIELD  | Identification of direction on the line |
| PROTOCOL       | Used IP protocol                        |
| TCP_FLAGS      | TCP protocol flags                      |
| TOS            | Type of service                         |
| TTL            | Remaining number of jumps               |

