import argparse
from ipaddress import IPv4Network, IPv4Address, AddressValueError
from pathlib import Path
from typing import List
import sys


def get_parsed_args() -> argparse.Namespace:
    """
    Initialize and parser ArgumentParser for command line arguments.
    :return: initialized ArgumentParser
    """
    parser = argparse.ArgumentParser(
        prog="get_unirec_filter.py",
        description="Loads file with input identifiers (IPv4 addresses and/or IPv4 networks) and generates "
                    "corresponding unirec filter, which can be used for filtering captured traffic with unirecfilter."
    )
    required_args = parser.add_argument_group("required arguments")
    optional_args = parser.add_argument_group("optional arguments")
    required_args.add_argument("-i", "--input", metavar="FILENAME", help="path to input list of identifiers",
                               required=True)
    optional_args.add_argument("-o", "--output", help="path to output of generated unirec filter")
    return parser.parse_args()


def parse_out_comments(lines: List[str], ignore: list = None) -> List[str]:
    """
    Removes comments from input (all lines).
    :param lines: lines of blacklist
    :param ignore: list of ignored chars (for example in hostname you want to allow '#'
    :return: cleaned lines

    :raise: TypeError if ignore arg is used and is not iterable
    """
    clean_lines = []

    comment_signs = {"#", "//"}
    if ignore is not None:
        for ignored_char in ignore:
            comment_signs.remove(ignored_char)

    for line in lines:
        if not line.strip():
            continue
        if line.startswith("#") or line.startswith("//"):
            continue
        if any(comment_sign in line for comment_sign in ("#", "//")):
            identifier = line.split('#')
            if len(identifier) == 1:
                identifier = line.split("//")
            clean_lines.append(identifier[0].strip())
            continue
        clean_lines.append(line.strip())
    return clean_lines


def get_input_lines(path_to_input_file: Path) -> list:
    """
    Loads input file with input identifiers.
    :param path_to_input_file: path to input file
    :return: list of loaded lines cleaned from comments
    """
    with path_to_input_file.open() as input_f:
        lines = input_f.readlines()
    cleaned_lines = parse_out_comments(lines)
    return cleaned_lines


def get_generated_filter_str(lines: list) -> str:
    """
    Generates filter for Unirec traffic capture tool.
    :param lines: cleaned loaded lines of input identifiers (ips and nets)
    :return: generated filter
    """
    filter_str = ""
    for identifier in lines:
        # validate syntax of identifier
        try:
            IPv4Address(identifier)
        except AddressValueError:
            try:
                IPv4Network(identifier)
            except AddressValueError:
                print(f"Cannot parse input identifier {identifier}! Only IPv4 and IPv4 network in CIDR notation is "
                      f"supported!")
                sys.exit(2)

        if filter_str != "":
            filter_str += " ||"
        filter_str += " SRC_IP == " + identifier + " || DST_IP == " + identifier
    return filter_str.strip()


if __name__ == "__main__":
    args = get_parsed_args()

    loaded_lines = get_input_lines(Path(args.input))
    generated_filter = get_generated_filter_str(loaded_lines)

    if args.output is None:
        print(generated_filter)
    else:
        with Path(args.output).open('w') as output_f:
            output_f.write(generated_filter)
