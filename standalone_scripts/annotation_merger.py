"""
Some errors may occur during annotation and it may be safer to split annotation of multiple networks into multiple
annotator runs. This way will the annotator generate multiple outputs but in final form you probably want just one
single annotation file as output. This script loads folder with multiple annotation files and merges them all into
one single final annotation file called 'annotation_merged.json'.
"""

from pathlib import Path
import json
import sys
import argparse


def parse_args():
    parser = argparse.ArgumentParser(
        prog="annotation_merger.py",
        description="Merges multiple annotation files into one."
    )
    optional_args = parser.add_argument_group("required arguments")
    optional_args.add_argument("-i", "--input", metavar="FOLDER", required=True,
                               help="Path to folder with multiple annotation files")
    return parser.parse_args()


args = parse_args()
input_path = Path(args.input)
if not input_path.is_dir():
    print("Specified input is not a folder!")
    sys.exit()

data = {}
for f in input_path.glob("*.json"):
    with f.open() as annotation_f:
        try:
            data.update(json.load(annotation_f))
        except json.JSONDecodeError:
            continue

with input_path.joinpath("annotation_merged.json").open('w') as result_f:
    json.dump(data, result_f, indent=4, sort_keys=True)
