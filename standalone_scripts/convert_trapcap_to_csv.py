"""
This script can be used for converting captured traffic data in .trapcap format into csv format. It has to be
started on collector-nemea.liberouter.org server (or some other server, where is installed nemea/logger).
"""
from pathlib import Path
import sys
import argparse
import os


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Transform collector's .trapcap files into CSV.")
    parser.add_argument("-i", "--input", default=str(Path().cwd()), help="Path to directory with trapcap files, "
                                                                         "script converts all .trapcap files in it")
    parser.add_argument("-o", "--output", default=str(Path().cwd()), help="Path to output folder for generated CSV "
                                                                          "files")
    parser.add_argument("-ds", "--date_start", default="00000000", help="Filter specific date start from input folder"
                        ", specified date is included. Expected format: YYYYMMDD")
    parser.add_argument("-de", "--date_end", default="99999999", help="Filter specific date end from input folder, "
                        "specified date is included, expected format: YYYYMMDD")
    parser.add_argument("-ts", "--time_start", default="0000", help="Filter specific time start from input folder, "
                        "specified time is included, expected format: HHMM. If used with date, the time filter is "
                        "applied to all dates")
    parser.add_argument("-te", "--time_end", default="9999", help="Filter specific time start from input folder, "
                        "specified time is included, expected format: HHMM.If used with date, the time filter is "
                        "applied to all dates")
    return parser


args = get_argument_parser().parse_args()
d_input = Path(args.input)
d_output = Path(args.output)
if not d_input.exists():
    print("Path to input folder does not exist!")
    sys.exit(2)
if not d_output.exists():
    print("Path to output folder does not exist!")
    sys.exit(2)

for filename in d_input.glob("*.trapcap.*"):
    try:
        prefix, suffix = str(filename.name).split(".trapcap.")
        file_date = int(suffix[:8])
        file_time = int(suffix[8:])
    except ValueError:
        # in case of "not enough values to unpack"
        continue
    if file_date < int(args.date_start):
        continue
    if file_date > int(args.date_end):
        continue
    if file_time < int(args.time_start):
        continue
    if file_time > int(args.time_end):
        continue
    output_path = d_output.joinpath(prefix + "." + suffix + ".csv")
    os.system(f"/usr/bin/nemea/logger -i f:{filename} -t >> {output_path}")
