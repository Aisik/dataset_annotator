"""
Acknowledgment:
Main idea of blacklist parsing comes from Cesnet's project NERD:
https://github.com/CESNET/NERD/blob/master/etc/blacklists.yml
The code used for parsing was refactored to object oriented parser factory.

COPYRIGHT AND PERMISSION NOTICE

Copyright (C) 2016-2019 CESNET, z.s.p.o.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the distribution.
  3. Neither the name of the Company nor the names of its contributors may
     be used to endorse or promote products derived from this software
     without specific prior written permission.

ALTERNATIVELY, provided that this notice is retained in full, this product
may be distributed under the terms of the GNU General Public License (GPL)
version 2 or later, in which case the provisions of the GPL apply INSTEAD OF
those given above.

This software is provided "as is", and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. In no event shall the
company or contributors be liable for any direct, indirect, incidental,
special, exemplary, or consequential damages (including, but not limited to,
procurement of substitute goods or services; loss of use, data, or profits;
or business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence or
otherwise) arising in any way out of the use of this software, even if
advised of the possibility of such damage.
"""
from ipaddress import IPv4Address, IPv4Network, AddressValueError, summarize_address_range
import re
from abc import ABC, abstractmethod
from logging import getLogger

from common import Configuration
from common.utils import get_modules_data_path
from input_processing import InputParser, Hostname
from .data_source_base import DataSourceBase, DataSourceLoader, log_successful_initialization


class Blacklists(DataSourceBase):
    """
    Data source, which checks, if IP address (or hostname) appears on some of the blacklists defined in configuration.
    """
    SUPPORTED_IDENTIFIER_TYPES = ("ip", "prefix", "domain")
    logger = getLogger("Blacklists")

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration().get_config_subset("enrichment.data_sources.blacklists", dict)

        # load configuration of all blacklists
        self.blacklists = []
        for bl_name, bl_conf in conf.items():
            assert bl_conf['identifier'] in self.SUPPORTED_IDENTIFIER_TYPES, \
                "Unsupported identifier type of blacklist " + bl_name + " in configuration!"
            self.blacklists.append({
                'name': bl_name,
                'url': bl_conf['url'],
                'label': bl_name + '/' + bl_conf['label'],
                'regex': bl_conf['regex'],
                'identifier': bl_conf['identifier']
            })

        # load all blacklists into blacklist['data'] attribute by either downloading it or loading it from file
        path_to_blacklists = get_modules_data_path().joinpath("blacklists")
        for bl in self.blacklists:
            lines = DataSourceLoader.load_source(path_to_blacklists.joinpath(bl['name']), bl['url'])
            bl['data'] = BlacklistParser.parse_all_records(InputParser.parse_out_comments(lines), bl['identifier'],
                                                           self.compile_regex(bl['regex']) if bl['regex'] else None)

    @staticmethod
    def compile_regex(regex: str) -> re.Pattern:
        """
        Replaces shortcuts by some often used regexes from blacklist conf
        :param regex: regular expression of blacklist
        :return: compiled blacklist
        """
        if "\\A" in regex:
            # replace "special" configuration character for IP address
            regex = regex.replace("\\A", "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}")
        if "\\P" in regex:
            # replace "special" configuration character to
            regex = regex.replace("\\P", "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(\/\d{1,2})?")
        return re.compile(regex)

    def _check_all_blacklists(self, ip: str, hostname: str) -> list:
        """
        Check if IP address or hostname of IP is on some blacklist. If yes, add it to label list, which will be used
        in final data_annotation structure.
        :param ip: currently processed ip
        :param hostname: hostname of currently processed ip address
        :return: list of blacklist names (labels), where IP or hostname appears
        """
        labels = []
        for bl in self.blacklists:
            if bl['identifier'] == "domain":
                if ".".join(hostname.split('.')[-2:]) in bl['data']:
                    labels.append(bl['label'])
            elif bl['identifier'] == "ip":
                if ip in bl['data']:
                    labels.append(bl['label'])
            elif bl['identifier'] == "prefix":
                for network in bl['data']:
                    if IPv4Address(ip) in network:
                        labels.append(bl['label'])
        return labels

    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        """
        Returns list of blacklist names, where ip or hostname occurs
        :param ip: currently processed ip
        :param hostname: currently processed hostname
        :return: updated data_annotation structure
        """
        return {
            'blacklists': self._check_all_blacklists(ip, hostname)
        }


class BlacklistParser:
    """
    Factory for creating parser based on its identifier type defined in configuration
    """
    @staticmethod
    def parse_all_records(records, identifier_type, regex) -> list:
        if identifier_type == "ip":
            return IpParser(regex).parse_all_records(records)
        elif identifier_type == "prefix":
            return PrefixParser(regex).parse_all_records(records)
        elif identifier_type == "domain":
            return DomainParser(regex).parse_all_records(records)


class Parser(ABC):
    """
    Abstract class for parsing blacklist records. Basically every blacklist can have configured regex or not. If regex
    is without regex, on all of its records will be called _parse() method.

    If regex is configured for blacklist, then _parse_w_regex() is called on all blacklist records. This method divides
    into two branches, which differ based on Parser type (child class). If regex has no group, or contains one or more
    groups decides, if _hadle_regex_wo_groups() or _hadle_regex_with_groups() will be called essentially.
    """
    logger = getLogger("blacklists - Parser")

    def __init__(self, regex):
        self.regex = regex

    def parse_all_records(self, records: list) -> list:
        """
        Define, which method should be called for parsing based on regex presence
        :param records: all blacklist records to be parsed
        :return: parsed records
        """
        if self.regex:
            return self._parse_w_regex(records)
        return self._parse(records)

    def _parse_w_regex(self, records: list) -> list:
        """
        If blacklist has configured regex, this method will be called for parsing.
        :param records: records to be parsed
        :return: list of parsed records
        """
        bl_records = []
        if self.regex.groups == 0:
            for rec in records:
                # if there are no groups in regex (most probably blacklist with multiple records on one line), try to
                # find all occurrences
                all_records = self.regex.finditer(rec)
                for ip_match in all_records:
                    try:
                        # for every occurrence found, check if IP address valid and add it to bl_records
                        record = ip_match[ip_match.span()[0]:ip_match.span()[1]]
                        parsed_record = self._hadle_regex_wo_groups(record)
                        if parsed_record:
                            bl_records.append(parsed_record)
                    except Exception:
                        self.logger.debug(f"Could not parse record: {ip_match}!")
        else:
            # there are on or more groups, process them
            for line in records:
                match = self.regex.search(line)
                if match:
                    try:
                        parsed_record = self._handle_regex_with_groups(match)
                        if parsed_record:
                            bl_records.append(parsed_record)
                    except Exception:
                        self.logger.debug(f"Could not parse record: {match}!")
        return bl_records

    @abstractmethod
    def _hadle_regex_wo_groups(self, record: str) -> list:
        """
        Handle processing of record without regex groups
        :param record: record to be parsed (validated)
        :return: parsed record
        """
        pass

    @abstractmethod
    def _handle_regex_with_groups(self, match: re.Match):
        """
        Handle processing of matched string with regex groups.
        :param match: matched string
        :return: parsed record
        """
        pass

    @staticmethod
    @abstractmethod
    def _parse(records: list) -> list:
        """
        Parse all records without regex.
        :param records: records to be parsed
        :return: list of parsed records
        """
        pass


class IpParser(Parser):
    """
    Parser for IP address parsing.
    """
    def _hadle_regex_wo_groups(self, record):
        return str(IPv4Address(record))

    def _handle_regex_with_groups(self, match: re.Match):
        return str(IPv4Address(match.group(1)))

    @staticmethod
    def _parse(records):
        parsed_records = []
        for ip in records:
            try:
                parsed_records.append(str(IPv4Address(ip)))
            except AddressValueError:
                continue
        return parsed_records


class PrefixParser(Parser):
    """
    Parser for network prefix parsing
    """
    def _hadle_regex_wo_groups(self, record):
        return IPv4Network(record)

    def _handle_regex_with_groups(self, match: re.Match):
        if self.regex.groups == 2:
            range_start = IPv4Address(match.group(1))
            range_end = IPv4Address(match.group(2))
            # create prefix from ip addr range
            prefix_in_cidr = [ipaddr for ipaddr in
                              summarize_address_range(range_start, range_end)][0]
            return prefix_in_cidr
        else:
            return IPv4Network(match.group(1))

    @staticmethod
    def _parse(records: list) -> list:
        parsed_records = []
        # validate prefix syntax
        for prefix in records:
            try:
                parsed_records.append(IPv4Network(prefix))
            except AddressValueError:
                continue
        return parsed_records


class DomainParser(Parser):
    """
    Parser for parsing domain names.
    """
    def _hadle_regex_wo_groups(self, record):
        if record.count('.') == 1 and Hostname.is_valid_hostname(record):
            return record

    def _handle_regex_with_groups(self, match: re.Match):
        if match.group(1).count('.') == 1 and Hostname.is_valid_hostname(match.group(1)):
            return match.group(1)

    @staticmethod
    def _parse(records: list) -> list:
        parsed_records = []
        for domain in records:
            if domain.count('.') == 1 and Hostname.is_valid_hostname(domain):
                parsed_records.append(domain)
        return parsed_records
