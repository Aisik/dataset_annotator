import json
import logging
from datetime import datetime, timedelta
import sys
import time

import requests
from censys.exceptions import CensysUnauthorizedException
from censys.ipv4 import CensysIPv4

from common import Configuration
from common.constants import TIME_STR_FORMAT
from common.utils import find_in_nested_dict, remove_all_NA_values, get_modules_data_path
from .data_source_base import DataSourceBase, wrap_ip_data, log_successful_initialization
from enrichment.processing_chains.runner import StopAndSaveData


class CensysConnector(DataSourceBase):
    """
    Connector, which downloads data from Censys service (censys.io).
    """
    logger = logging.getLogger("CensysConnector")
    UPDATED_AT_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
    API_URL = "https://censys.io/api/v1/view/ipv4/"

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration().get_config_subset("enrichment.data_sources.censys", dict)

        try:
            self.api = CensysIPv4(conf['api_id'], conf['secret'])
        except KeyError:
            self.logger.error("Missing api_id or secret in configuration!")
            sys.exit(2)
        except CensysUnauthorizedException:
            self.logger.error("Unable to authorize to API, api_id or secret is misconfigured!")
            sys.exit(2)
        self.api_id = conf['api_id']
        self.secret = conf['secret']
        self.rate_limiter_enabled = conf['enable_rate_limiter']
        self.action_performed_counter = 0
        self.api_timer = datetime.now()

        try:
            with get_modules_data_path().joinpath("censys", "censys_scanned_ports.json").open() as ports_f:
                self.scanned_ports = set(json.load(ports_f))
        except Exception as e:
            self.logger.error(str(e))
            self.logger.error("Error occurred while loading list of scanned ports!")
            sys.exit(2)

    def close(self) -> None:
        """
        Save updated list of Censys's scanned ports, since available list on Censys websites is not accurate.
        :return: None
        """
        with get_modules_data_path().joinpath("censys", "censys_scanned_ports.json").open('w') as ports_f:
            json.dump(list(self.scanned_ports), ports_f)

    @staticmethod
    def _find_ports_banner(port_info: dict) -> str:
        """
        Try to find banner in ports dictionary from response (it has unstable structure, which differs from port to
        port)
        :param port_info: port info to be searched
        :return: found banner
        """
        banner = find_in_nested_dict("banner", port_info)
        # banner can be directly str or in dict structure under 'raw' key
        if isinstance(banner, dict):
            return banner.get('raw', banner.get('banner_decoded', ""))
        if banner is None:
            return "NA"
        return banner

    @classmethod
    def _parse_port_info(cls, port_info: dict) -> dict:
        """
        Get all relevant info from port's data.
        :param port_info: data about port
        :return: relevant data (banner and product's info)
        """
        keys = ("product", "version", "description", "manufacturer")

        obtained_data = find_in_nested_dict("metadata", port_info)

        data = {'banner': cls._find_ports_banner(port_info)}
        if obtained_data is not None:
            for k in keys:
                if k != "product":
                    data[k] = obtained_data.get(k, "NA")
                else:
                    data['name'] = obtained_data.get(k, "NA")
        return data

    def _parse_applications(self, response: dict) -> dict:
        """
        Parse application info from all ports in response.
        :param response: response from Censys's API
        :return: parsed products info
        """
        applications = {}
        for port in response['ports']:
            clean_port_info = remove_all_NA_values(self._parse_port_info(response[str(port)]))
            if clean_port_info:
                applications[port] = clean_port_info
        return applications

    def _parse_response(self, response: dict) -> dict:
        """
        Parses all relevant data from Censys's API response on requsted IP.
        :param response: response from API
        :return: parsed relevant data
        """
        # update list of Censys's scanned ports, since available list on Censys websites is not accurate
        for port in response['ports']:
            self.scanned_ports.add(port)

        return {
            'tags': response.get('tags', []),
            'open_ports': response['ports'],
            'applications': self._parse_applications(response),
            'protocols': response['protocols'],
            'geographic_location': response.get('location', "NA"),
            # convert str date to datetime object and then back into common str format
            'timestamp': datetime.strptime(response['updated_at'], self.UPDATED_AT_FORMAT).strftime(TIME_STR_FORMAT),
            'metadata': response.get('metadata', {}),
            'as': response.get('autonomous_system')
        }

    def _check_api_rate_limit(self):
        """
        Naive API rate limit checker. Checks API limit base on allowed limit of Censys free account, which is
        0.4 actions/second (120 seconds/5 minutes). The checker uses 115 actions because 120 could be late so 5 actions
         is just reserve.
        :return:
        """
        if self.action_performed_counter == 0:
            self.api_timer = datetime.now()
            return

        if self.action_performed_counter < 115:
            return

        if self.action_performed_counter >= 115:
            now = datetime.now()
            diff = now - self.api_timer
            if diff < timedelta(minutes=5):
                sleep_seconds = (timedelta(minutes=5) - diff).seconds
                self.logger.info(f"Going to sleep for {sleep_seconds} seconds, because API rate limit is almost "
                                 f"reached!")
                time.sleep(sleep_seconds)
            self.logger.info("Resetting naive rate limit checker counters!")
            self.action_performed_counter = 0
            self.api_timer = datetime.now()

    @wrap_ip_data("censys")
    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        """
        Collect data for requested IP.
        :param ip: requested IP
        :param hostname: hostname of IP
        :return: relevant data from Censys

        :raises: StopAndSaveData
        """
        self.logger.debug(f"Processing ip: {ip}")

        if self.rate_limiter_enabled:
            self._check_api_rate_limit()

        try:
            res = requests.get(self.API_URL + ip, auth=(self.api_id, self.secret))
        except Exception as e:
            self.logger.error(e)
            self.logger.error("Some problem occurred while querying data from Censys server!")
            return {'error': "Some problem occurred while querying data from Censys server!"}

        self.action_performed_counter += 1
        if res.status_code == 200:
            data = json.loads(res.text)
            return self._parse_response(data)
        elif res.status_code == 429:
            error = json.loads(res.text)
            if error.get('error_type') == "quota_exceeded":
                self.logger.error("Censys API quota limit exceeded!")
                raise StopAndSaveData

            self.logger.info("Going to sleep for 3 s and will try to process IP again, because API rate limit was "
                             "reached and naive rate limit checker did not help!")
            time.sleep(3)
            return self.get_data_for_ip(ip)
        elif res.status_code == 404:
            self.logger.debug(f"No data available for ip {ip}!")
            return {'error': "No data available!"}
