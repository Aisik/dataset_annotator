from ipwhois import IPWhois
from logging import getLogger

from .data_source_base import DataSourceBase, wrap_ip_data, log_successful_initialization


class WhoIs(DataSourceBase):
    """
    Data source downloading relevant data about IP address from WhoIs service.
    """
    logger = getLogger("WhoIs")

    @log_successful_initialization(logger)
    def __init__(self):
        pass

    @staticmethod
    def _parse_address_and_mail(whois_data: dict):
        res = {
            'address': "NA",
            'email': "NA"
        }
        for obj, data in whois_data['objects'].items():
            try:
                res['address'] = data['contact']['address'][0]['value']
                res['email'] = data['contact']['email']
                break
            except KeyError:
                pass
        return res

    @classmethod
    def _parse_data(cls, whois_data: dict) -> dict:
        addr_mail = cls._parse_address_and_mail(whois_data)
        return {
            'asn': whois_data['asn'],
            'asn_cidr': whois_data['asn_cidr'],
            'asn_description': whois_data['asn_description'],
            'entities': whois_data['entities'],
            'address': addr_mail['address'],
            'email': addr_mail['email']
        }

    @wrap_ip_data("whois")
    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        try:
            data = IPWhois(ip).lookup_rdap()
        except Exception as e:
            self.logger.warning(f"Error while querying ip {ip}!")
            return {'error': str(e)}

        return self._parse_data(data)
