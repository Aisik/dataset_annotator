import requests
from datetime import datetime, timedelta
import logging
import json
from typing import Dict

from common import Configuration
from .data_source_base import DataSourceBase, wrap_ip_data, log_successful_initialization


class AdictConnector(DataSourceBase):
    OPEN_PORTS_ATTR = "open_ports"
    ATTR_ENDPOINT = "/ip/{ip}/{attr}/history"
    MAX_AGE = timedelta(days=7)
    logger = logging.getLogger("AdictConnector")

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration()

        self.api_url = conf.get_config_subset("enrichment.data_sources.adict.url", str)
        if self.api_url.endswith('/'):
            # remove last '/', it is already contained in ENDPOINT string
            self.api_url = self.api_url[:-1]

    @classmethod
    def _get_endpoint_path(cls, ip, attr: str) -> str:
        """
        Construct GET API endpoint link for ip address and database attribute.
        """
        return cls.ATTR_ENDPOINT.format(ip=ip, attr=attr)

    def _handle_error(self, ip: str, response: requests.Response, ip_data: dict) -> None:
        """
        Handle error response from request to ADiCT database.
        :param ip: Requested IP
        :param response: error response to handle
        :param ip_data: dict holding IP data, can be updated directly
        :return: None
        """
        if response.status_code == 404 and response.text.startswith("No records found for ip/"):
            ip_data['error'] = "No record available for this IP!"
            self.logger.debug(f"No record available for ip {ip} and attribute {self.OPEN_PORTS_ATTR}.")
        else:
            ip_data['error'] = "Something went wrong while querying API!"
            self.logger.warning(f"Something went wrong while querying ADiCT with IP address {ip} and attribute"
                                f" {self.OPEN_PORTS_ATTR}!")

    @staticmethod
    def _get_all_unique_values_from_data_points(data_points: list) -> list:
        """
        Applies intersection on all values of input data points.
        :param data_points: list of data points to be merged
        :return: intersection of input data points
        """
        values = set()
        for data_point in data_points:
            values.update(set(data_point['v']))
        return list(values)

    def _get_open_ports(self, ip, ip_data: dict) -> None:
        """
        Try to decode successful answer from ADiCT database.
        :param ip: Requested IP
        :param ip_data: dict holding IP data, can be updated directly
        :return: None
        """
        response = requests.get(self.api_url + self._get_endpoint_path(ip, self.OPEN_PORTS_ATTR), params={
            't1': str(datetime.now() - self.MAX_AGE),
            't2': str(datetime.now())
        })
        self.logger.info(f"Request finished!")
        if response.status_code != 200:
            self._handle_error(ip, response, ip_data)
        else:
            try:
                ip_data[self.OPEN_PORTS_ATTR] = self._get_all_unique_values_from_data_points(json.loads(response.text))
            except json.JSONDecodeError:
                ip_data['error'] = "Could not decode data from API!"
                self.logger.warning(f"Something went wrong while decoding data from ADiCT with IP address {ip} and "
                                    f"attribute {self.OPEN_PORTS_ATTR}!")

    @wrap_ip_data("adict")
    def get_data_for_ip(self, ip: str, hostname: str) -> Dict[str, dict]:
        """
        Get data about requested IP through ADiCT's API.
        :param ip: requested IP address
        :param hostname: hostname of IP
        :return: Dictionary containing received and processed data.
        """
        self.logger.debug(f"Processing ip: {ip}.")

        ip_data = {}
        self._get_open_ports(ip, ip_data)

        self.logger.debug(f"Finished processing of ip {ip}.")
        return ip_data
