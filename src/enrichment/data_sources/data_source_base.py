from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from functools import wraps
from logging import getLogger, Logger
from pathlib import Path
from typing import Iterable

import requests
import sys


class DataSourceBase(ABC):
    """
    This class servers as base abstract class for every enrichment data source (module). If any data source module
    does not inherit from this class, it will not be loaded by dataset annotator!! This way the automatic loading of
    enabled data sources in configuration is guaranteed.
    """
    def close(self) -> None:
        """
        This method runs, when all data are processed and should be used as finalization method (destructor)
        :return: None
        """
        pass

    @abstractmethod
    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        """
        Gets data from datasource for currently processed IP. It is the entry point for every data source.
        :param ip: currently processed IP
        :param hostname: hostname of currently processed IP
        :return: data obtained from data source

        :raises StopAndSave: may raise StopAndSave exception from enrichment.runner
        """
        pass


class DataSourceLoader:
    logger = getLogger("DataSourceLoader")

    @classmethod
    def load_source(cls, path_to_source: Path, url: str) -> list:
        """
        Load source from file. If file does not exist, create source file by downloading it.
        :param path_to_source: path in os to existing source file
        :param url: url for downloading data source in case the file does not exist
        :return: loaded data source lines
        """
        try:
            with path_to_source.open() as nodes_list_f:
                nodes_list = [node.strip() for node in nodes_list_f.readlines()]

                list_download_datetime = datetime.fromtimestamp(path_to_source.stat().st_ctime)
                if list_download_datetime < datetime.now() - timedelta(days=1):
                    # if downloaded list of nodes is older than one day, download it again (act as if file did
                    # not exist)
                    raise FileNotFoundError
        except FileNotFoundError:
            nodes_list = cls._create_source_file(url, path_to_source)
        return nodes_list

    @classmethod
    def _create_source_file(cls, url, path_to_list: Path) -> list:
        """
        Download source lines and write them to file.
        :param url: URL to source
        :param path_to_list: path, where will be the source saved
        :return: downloaded source lines
        """
        lines = cls._download_source(url)

        with path_to_list.open('w') as all_nodes_f:
            for node in lines:
                all_nodes_f.write(node + "\n")
        return lines

    @classmethod
    def _download_source(cls, url: str) -> list:
        """
        Try to download source and return downloaded lines.
        :param url: URL to source
        :return: downloaded lines of source
        """
        response = requests.get(url)
        if response.status_code == 200:
            return response.text.split('\n')
        else:
            cls.logger.error(f"Could not download source list ({url}), because some error "
                             f"occurred: {response.text}")
            sys.exit(2)


def wrap_ip_data(data_source_name, keys_to_root: Iterable = None):
    """
    Decorator func, which wraps data into dict under data_source_name_key and can move some keys to wrapper dict.
    Transforms this (if keys_to_root=['Y']:
    {
        'X': ...,
        'Y': ...
    }
    into this:
    {
        data_source_name: {
            'X': ...
        }
        'Y': ...
    }
    :param data_source_name: name of key to
    :param keys_to_root: List (or any other iterable) of keys, which are moved to from data source dict to root
                         structure
    :return: wrapped function
    """
    def wrap(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            wrapped_dict = {
                data_source_name: func(*args, **kwargs)
            }
            if keys_to_root is not None and wrapped_dict[data_source_name].get("error") is None:
                try:
                    for key in keys_to_root:
                        try:
                            wrapped_dict[key] = wrapped_dict[data_source_name][key]
                            wrapped_dict[data_source_name].pop(key)
                        except KeyError:
                            logger = getLogger("ip_data_wrapper")
                            logger.warning(f"Could not move key {key} to root of annotation structure!")
                except TypeError:
                    # keys_to_root arg is not iterable
                    pass
            return wrapped_dict
        return wrapped
    return wrap


def log_successful_initialization(logger: Logger):
    """
    Decorator, which should be used on __init__() method of each data source to log successful initialization.
    :param logger: logger object of data source, which will be used for logging
    :return: wrapped function
    """
    def wrap(func):
        @wraps(func)
        def wrapped_func(*args, **kwargs):
            logger.debug("Starting initialization!")
            func(*args, **kwargs)
            logger.debug("Successfully initialized!")
        return wrapped_func
    return wrap
