from logging import getLogger

from common.utils import get_modules_data_path
from common import Configuration
from .data_source_base import DataSourceBase, DataSourceLoader, log_successful_initialization


class Tor(DataSourceBase):
    """
    Data source, which checks, if the ip is TOR node.
    """
    logger = getLogger("Tor")

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration().get_config_subset("enrichment.data_sources.tor")

        data_path = get_modules_data_path()
        # load all TOR nodes list (basic nodes and exit nodes) from file, if file does not exist or data are too old,
        # try to download actual data
        url_all_nodes = conf['url_all_nodes']
        self.all_nodes = []
        path_all_nodes = data_path.joinpath("tor", "tor_all_nodes.txt")
        self.all_nodes = DataSourceLoader.load_source(path_all_nodes, url_all_nodes)

        url_exit_nodes = conf['url_exit_nodes']
        self.exit_nodes = []
        path_exit_nodes = data_path.joinpath("tor", "tor_exit_nodes.txt")
        self.exit_nodes = DataSourceLoader.load_source(path_exit_nodes, url_exit_nodes)

    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        data = {}
        if ip in self.exit_nodes:
            data['labels'] = ["TOR_exit_node"]
        elif ip in self.all_nodes:
            data['labels'] = ["TOR_node"]

        return data
