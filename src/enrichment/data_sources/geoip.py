"""
Acknowledgment:
This product includes GeoLite2 data created by MaxMind, available from https://www.maxmind.com
"""
import sys
import tarfile
from pathlib import Path
from logging import getLogger

from geoip2 import database, errors
from geoip2.models import City
import wget

from .data_source_base import DataSourceBase, wrap_ip_data, log_successful_initialization
from common import Configuration
from common.utils import get_modules_data_path


class GeoIp(DataSourceBase):
    """
    Data source downloading data from geolocation database (currently using MaxMind geolocation database). Downloading
    of database locally is automatically handled by this data source.
    """
    logger = getLogger("GeoIp")
    DB_ARCHIVE_NAME = "geoip.tar.gz"

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration().get_config_subset("enrichment.data_sources.geoip")
        path_to_db = get_modules_data_path().joinpath("geoip", "GeoLite2-City.mmdb")
        if not path_to_db.exists():
            self._download_database(conf['url'], conf['key'], path_to_db)
            self._extract_db_archive(path_to_db.parent.joinpath(self.DB_ARCHIVE_NAME))
        self._reader = database.Reader(str(path_to_db))

    def _download_database(self, url: str, key: str, path_to_db: Path) -> None:
        """
        Download MaxMind GeoLite-City database.
        :param url: url of database archive
        :param key: user's license key
        :param path_to_db: path, where will be downloaded db archive stored
        :return: None
        """
        # fill license key into database download link
        key_index = url.find('license_key=') + len("license_key=")
        url_w_key = url[:key_index] + key + url[key_index:]
        archive_db_path = path_to_db.parent.joinpath(self.DB_ARCHIVE_NAME)
        try:
            self.logger.info("Going to download GeoLite2-City database file, it may take few seconds...")
            wget.download(url_w_key, str(archive_db_path))
            self.logger.info("Database downloaded successfully!")
        except Exception as e:
            self.logger.error("Error occurred while downloading MaxMind GeoLite2-City database!")
            self.logger.error(e)
            sys.exit(2)

    def _extract_db_archive(self, db_archive_path: Path) -> None:
        """
        Extracts db file from downloaded database archive.
        :param db_archive_path: path to downloaded db archive
        :return: None
        """
        try:
            with tarfile.open(str(db_archive_path), "r:gz") as tar_f:
                for member in tar_f.getmembers():
                    # go through every file (directory) in archive and find database file in it
                    if member.name.endswith("GeoLite2-City.mmdb"):
                        db_file_member = member
                        break
                tar_f.makefile(db_file_member, db_archive_path.parent.joinpath("GeoLite2-City.mmdb"))
        except Exception as e:
            self.logger.error("Error occurred while downloading and extracting MaxMind GeoLite2-City database!")
            self.logger.error(e)
            sys.exit(2)
        # remove downloaded archive
        db_archive_path.unlink()

    @staticmethod
    def _parse_data(db_result: City) -> dict:
        """
        Parses data from record in database.
        :param db_result: found record in database based on currently processed IP
        :return: parsed data
        """
        return {
            'city': db_result.city.names.get('en', "NA"),
            'country': db_result.country.iso_code,
            'timezone': db_result.location.time_zone,
            'latitude': db_result.location.latitude,
            'longitude': db_result.location.longitude
        }

    @wrap_ip_data("geoip")
    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        """
        Query IP address in GeoLite2-City database and parse found record.
        :param ip currently processed IP
        :param hostname: hostname of IP
        :return: parsed geolocation data
        """
        try:
            res = self._reader.city(ip)
        except errors.AddressNotFoundError:
            self.logger.debug("IP address not found in GeoIP database!")
            return {'error': "IP address not found in GeoIP database"}

        return self._parse_data(res)
