import logging
from datetime import datetime

import sys
import time
from shodan import Shodan, exception

from common import Configuration
from common.constants import TIME_STR_FORMAT
from common.utils import remove_all_NA_values
from .data_source_base import DataSourceBase, wrap_ip_data, log_successful_initialization


class ShodanConnector(DataSourceBase):
    """
    Data source downloading useful data from Shodan service (shodan.io).
    """
    logger = logging.getLogger("ShodanConnector")
    LAST_UPDATE_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration().get_config_subset("enrichment.data_sources.shodan", dict)

        try:
            self.api = Shodan(conf['api_key'])
            self.api.info()
        except KeyError:
            self.logger.error(f"API key is not configured!")
            sys.exit(2)
        except exception.APIError:
            self.logger.error("Invalid API key!")
            sys.exit(2)

    def _get_product(self, port_data: dict) -> dict:
        """
        Get product name from port data of Shodan query
        :param port_data: port data of Shodan query
        :return: None
        """
        product_info = {
            'name': port_data.get('product', "NA"),
            'version': port_data.get('version', "NA"),
            'device_type': port_data.get('devicetype', "NA"),
            'info': port_data.get('info', "NA"),
            'banner': port_data.get('data', "NA"),
            'timestamp': datetime.strptime(port_data['timestamp'], self.LAST_UPDATE_FORMAT).strftime(TIME_STR_FORMAT)
        }
        return remove_all_NA_values(product_info)

    def _get_query_response(self, ip: str) -> dict:
        """
        Request IP data from Shodan's API.
        :param ip: requested IP
        :return: response from API
        """
        try:
            return self.api.host(ip)
        except exception.APIError as e:
            if e.value == "No information available for that IP.":
                return {'error': e.value}
            elif e.value.startswith("Request rate limit reached"):
                self.logger.debug("Rate limit reached, going to sleep for 1 sec and will try again!")
                time.sleep(1)
                return self.get_data_for_ip(ip)
            else:
                self.logger.warning(f"Error occurred while getting data for ip {ip}!")
                self.logger.warning(e)
                return {'error': e.value}

    @classmethod
    def _get_open_ports(cls, sh_ports_data: list) -> dict:
        open_ports = {}
        for port_data in sh_ports_data:
            open_ports[port_data['port']] = datetime.strptime(port_data['timestamp'],
                                                              cls.LAST_UPDATE_FORMAT).strftime(TIME_STR_FORMAT)
        return open_ports

    @classmethod
    def _parse_response(cls, sh_data: dict) -> dict:
        """
        Parses all useful data from shodan's data.
        :param sh_data: Shodan's data obtained from Shodan's API
        :return: parsed Shodan's data
        """
        return {
            'open_ports': cls._get_open_ports(sh_data['data']),
            'tags': sh_data['tags'],
            'os': sh_data['os'],
            'applications': {},
            'geographic_location': {'city': sh_data['city'], 'latitude': sh_data['latitude'],
                                    'longitude': sh_data['longitude']},
            # convert str date to datetime object and then back into common str format
            'timestamp': datetime.strptime(sh_data['last_update'], cls.LAST_UPDATE_FORMAT).strftime(TIME_STR_FORMAT),
            'vulnerabilities': sh_data.get('vulns', [])
        }

    @wrap_ip_data("shodan", ["vulnerabilities"])
    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        """
        Query Shodan with IP address and save all useful data from the query result
        :param ip: requested ip
        :param hostname: hostname of IP
        :return: None
        """
        self.logger.debug(f"Processing ip: {ip}")

        sh_data = self._get_query_response(ip)
        if not sh_data.get('ip'):
            # key 'ip' is in every standard response of Shodan's API, so some error had to occur, return it
            return sh_data

        useful_data = self._parse_response(sh_data)

        # try to get product info from data
        for port_data in sh_data['data']:
            clean_port_data = self._get_product(port_data)
            if clean_port_data:
                useful_data['applications'][port_data['port']] = clean_port_data

        return useful_data
