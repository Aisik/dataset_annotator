import json
from datetime import datetime, timedelta
from logging import getLogger

import requests

from .data_source_base import DataSourceBase, wrap_ip_data, log_successful_initialization
from common import Configuration


class PassiveDNS(DataSourceBase):
    """
    Data source using Cesnet's Passive DNS located at https://passivedns.cesnet.cz/. For access contact krkos@cesnet.cz.
    """
    logger = getLogger("PassiveDNS")
    DATETIME_FMT = "%Y-%m-%dT%H:%M:%S.%f"
    MAX_AGE = timedelta(days=7)

    @log_successful_initialization(logger)
    def __init__(self):
        conf = Configuration().get_config_subset("enrichment.data_sources.pdns")
        self.url = conf['url']
        self.auth_token = conf['token']

    def _has_details_low_ttl(self, details_url) -> bool:
        """
        Query details of Passive DNS record and check, if one of the cached records did not have low TTL (< 300).
        :param details_url: url of record details
        :return: True or False if record had low TTL
        """
        resp_detail = requests.get(details_url, params={'token': self.auth_token})

        if resp_detail.status_code == 200:
            details = json.loads(resp_detail.content.decode())
            for data in details:
                try:
                    if data['ttl'] < 300:
                        return True
                except KeyError:
                    pass
        else:
            self.logger.warning(f"Could not obtains details!")
        return False

    def _has_low_ttl(self, domains: dict) -> bool:
        """
        For every domain check, if it does not have some record with low TTL. But first check, if the domain was
        queried lower or equal than 10 times.
        :param domains: dict of domain names under key and as value is dict with details_url list
        :return: True if there is some low TTL in records, else False
        """
        for domain, data in domains.items():
            if data['count'] <= 10:
                try:
                    for details_url in data['details_url']:
                        if self._has_details_low_ttl(details_url):
                            return True
                except KeyError:
                    print()
        return False

    def _parse_data_from_response(self, resp: requests.Response) -> dict:
        """
        Parse data about domains from Passive DNS's response.
        :param resp: response from Passive DNS's API
        :return: info about domains (domain names as key and count, details_url dict as value
        """
        if resp.status_code == 200:
            content = json.loads(resp.content.decode())
            domains = {}
            for record in content:
                if datetime.strptime(record['time_last'], self.DATETIME_FMT) < (datetime.now() - self.MAX_AGE):
                    # record is outdated
                    continue
                if record['rtype'] == 'A':
                    domain_name = record['domain']
                    if domain_name.endswith("in-addr.arpa") or domain_name.endswith("ip6.arpa"):
                        # sometimes in A records appear records with PTR domain, skip those
                        continue
                    domains[domain_name] = {}
                    try:
                        domains[domain_name]['count'] += record['count']
                        domains[domain_name]['details_url'].append(record['details'])
                    except KeyError:
                        domains[domain_name]['count'] = record['count']
                        domains[domain_name]['details_url'] = [record['details']]
            return domains
        elif resp.status_code == 404:
            return {'error': "No data available for this IP!"}
        else:
            self.logger.warning(f"Error in parsing response!")
            self.logger.warning(resp.text)
            return {'error': resp.text}

    def _get_domains_info(self, domains: dict) -> dict:
        """
        Get info from domains structure and get it into final data_annotation form.
        :param domains: parsed info about domains from passive DNS's API
        :return: final data_annotation structure
        """
        if "error" in domains.keys():
            return domains

        labels = []
        if self._has_low_ttl(domains):
            labels.append("low_ttl")

        return {
            'domain_count': len(domains.keys()),
            'labels': labels
        }

    @wrap_ip_data("pdns")
    def get_data_for_ip(self, ip: str, hostname: str) -> dict:
        """
        Query passive DNS's API and parse relevant data out of it.
        :param ip: currently processed IP
        :param hostname: currently processed hostname
        :return: final data_annotation structure
        """
        self.logger.debug(f"Processing ip: {ip}")

        try:
            resp = requests.get(self.url + "/api/v1/ip/" + ip, params={'token': self.auth_token})
        except Exception:
            self.logger.warning(f"Could not obtain data for ip {ip}")
            return {'error': "Error while querying data!"}

        parsed_domains_data = self._parse_data_from_response(resp)
        return self._get_domains_info(parsed_domains_data)
