from .integrator_base import IntegratorBase


class Tags(IntegratorBase):
    """
    This integrator integrates together tags from Censys and Shodan.
    """
    USED_DATA_SOURCES = ("shodan", "censys")

    def __init__(self):
        super().__init__()

    def run_integration(self, ip: str, ip_data: dict) -> dict:
        tags = set()
        for data_source in self.available_data_sources:
            tags.update(ip_data[data_source].get('tags', {}))

        ip_data['external_tags'] = list(tags)
        return ip_data
