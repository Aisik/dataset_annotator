import csv
import json
import logging
from copy import copy
from datetime import timedelta, datetime
from typing import Tuple

from shodan import Shodan
from whatportis import get_ports

from common.constants import TIME_STR_FORMAT
from common.utils import get_modules_data_path
from .integrator_base import IntegratorBase


class OpenPorts(IntegratorBase):
    """
    Integrator, which takes lists of open ports from relevant data sources and integrates them into lists based on
    data sources reliability. Basically all open ports from ADiCT are taken and open ports from Shodan and Censys are
    taken too, but these ports gets assigned certain confidence level (high|medium|low).
    """

    logger = logging.getLogger("OpenPortsIntegrator")
    USED_DATA_SOURCES = {"adict", "shodan", "censys"}
    MAX_RECORD_AGE = timedelta(days=7)
    CENSYS_SCANNED_PORTS_PATH = get_modules_data_path().joinpath("censys", "censys_scanned_ports.json")
    SCANNERS_CONFIDENCE_LEVELS = {
        'high': "high_confidence",
        'medium': "medium_confidence",
        'low': "low_confidence"
    }

    def __init__(self):
        super().__init__()
        self.include_service_description = self.conf['integrators']['open_ports']['include_service_description']
        self.common_scanned_ports = self._get_common_scanned_ports()
        self.port_to_service = PortToService()

    @staticmethod
    def _add_to_common_scanned_ports(common_ports: set, new_ports: set) -> set:
        if not common_ports:
            return new_ports
        else:
            return common_ports.intersection(new_ports)

    def _get_common_scanned_ports(self) -> set:
        """
        Load list of scanned ports, which do scan the global scanners (shodan and adict)
        :return: list of common scanned ports
        """
        common_scanned_ports = set()
        if "shodan" in self.available_data_sources:
            shodan_api = Shodan(self.conf['data_sources']['shodan']['api_key'])
            common_scanned_ports = self._add_to_common_scanned_ports(common_scanned_ports, set(shodan_api.ports()))

        if "censys" in self.available_data_sources:
            with self.CENSYS_SCANNED_PORTS_PATH.open() as f:
                scanned_ports = json.load(f)
                assert isinstance(scanned_ports, list), "File with Censys scanned ports must contain only list of " \
                                                        "ports!"
                common_scanned_ports = self._add_to_common_scanned_ports(common_scanned_ports, set(scanned_ports))

        return common_scanned_ports

    def _are_data_valid(self, data_source_name: str, data: dict) -> bool:
        """
        Check if data from data source does not contain any error and are not too old (based on timestamp attr)
        :param data_source_name: name of data source
        :param data: data from data source
        :return: True if data does not contain any error and are not too old, else False
        """
        if data.get('error') is not None:
            return False

        # default ADiCT's age of attribute to be valid is 7 days
        if data_source_name == "adict":
            return True

        # for other sources check age of data (at least their last_updated timestamp)
        try:
            if datetime.strptime(data['timestamp'], TIME_STR_FORMAT) > datetime.today() - self.MAX_RECORD_AGE:
                return True
        except KeyError:
            self.logger.warning("Could not load timestamp of data source!")
        return False

    def _get_integrated_ports(self, open_ports: list, ip_data: dict, valid_data_sources: set) -> Tuple[set, dict]:
        """
        Calculates all ports, which should be integrated from united list of open ports from all sources
        :param open_ports: list of all open ports from all sources
        :param ip_data: data about current IP address
        :return: set of integrated ports and dict of ports from scanners with confidence labels
        """
        integrated_ports = set()
        open_ports_scanners = {}
        for port in open_ports:
            if "adict" in valid_data_sources and port in ip_data['adict'].get('open_ports', []):
                integrated_ports.add(port)
                continue

            # censys and shodan are both used
            if all(source in valid_data_sources for source in ("shodan", "censys")):
                if port in ip_data['shodan']['open_ports'].keys() and port in ip_data['censys']['open_ports']:
                    # if port in shodan and censys, use it
                    open_ports_scanners[port] = self.SCANNERS_CONFIDENCE_LEVELS['high']
                else:
                    if port in self.common_scanned_ports:
                        open_ports_scanners[port] = self.SCANNERS_CONFIDENCE_LEVELS['low']
                    else:
                        open_ports_scanners[port] = self.SCANNERS_CONFIDENCE_LEVELS['medium']
            else:
                # only censys or shodan are used
                open_ports_scanners[port] = self.SCANNERS_CONFIDENCE_LEVELS['medium']
        return integrated_ports, open_ports_scanners

    def _get_valid_data_sources(self, ip_data: dict) -> set:
        """
        Check if data from data source are not too old or does not contain error.
        :param ip_data: data about IP
        :return: set of valid sources
        """
        valid_data_sources = copy(self.available_data_sources)
        for data_source in self.available_data_sources:
            if not self._are_data_valid(data_source, ip_data[data_source]):
                valid_data_sources.remove(data_source)
        return valid_data_sources

    @classmethod
    def _get_united_ports(cls, valid_data_sources: set, ip_data: dict) -> set:
        """
        Unite port numbers from all sources. It means, check if Shodan's port info is not too old and if not, add it
        to union. Other sources just add straight to union.
        :param valid_data_sources: data sources, which can be used for integration
        :param ip_data: data about IP
        :return: list of united port numbers
        """
        open_ports = set()
        for data_source in valid_data_sources:
            if data_source == "shodan":
                outdated_ports = []
                for port in ip_data[data_source].get('open_ports', {}).keys():
                    # Shodan has timestamp for every port
                    if ip_data[data_source]['open_ports'][port] > (datetime.now() - cls.MAX_RECORD_AGE).strftime(
                            TIME_STR_FORMAT):
                        open_ports.add(port)
                    else:
                        outdated_ports.append(port)
                # remove outdated ports from ports data structure
                for port in outdated_ports:
                    ip_data[data_source]['open_ports'].pop(port)
            else:
                for port in ip_data[data_source].get('open_ports', []):
                    open_ports.add(port)
        return open_ports

    def _obtain_ports_info(self, integrated_ports: list, ip_data: dict) -> None:
        """
        Get info about every port from integrated ports list.
        :param integrated_ports: final ports, which were integrated
        :param ip_data: data about current IP
        :return: None - info is updated through ip_data arg
        """
        device_types = set()
        # if user does not want include description, then return just list of services, otherwise return dictionary,
        # where key is service name and and as value is dict containing port number and description of the service
        if self.include_service_description:
            services = {}
        else:
            services = []
        for port in integrated_ports:
            # list of dicts describing services runned on the port
            port_info = self.port_to_service.get_port_info(port)
            for info in port_info:
                try:
                    # check if port was not translated into general label (device type)
                    device_types.update(info['labels'])
                except KeyError:
                    pass
                if self.include_service_description:
                    # update info structure so it contains only port number and description
                    info.pop('labels', None)
                    info['port'] = port
                    service_name = info.pop('service')
                    if service_name not in services.keys():
                        services[service_name] = info
                else:
                    services.append(info['service'])

        ip_data['device_type'] = list(device_types)
        ip_data['services'] = services

    def run_integration(self, ip: str, ip_data: dict) -> dict:
        """
        Run integration of port number on data from relevant data sources (ADiCT, Shodan, Censys)
        :param ip: processed IP address
        :param ip_data: data about IP, which will be used for integration
        :return: integrated data
        """
        self.logger.debug(f"Processing ip {ip}!")

        valid_data_sources = self._get_valid_data_sources(ip_data)

        united_open_ports = self._get_united_ports(valid_data_sources, ip_data)

        # integrate united ports from all sources
        integrated_ports, ports_scanners = self._get_integrated_ports(sorted(list(united_open_ports)), ip_data,
                                                                      valid_data_sources)
        ip_data['open_ports'] = sorted(list(integrated_ports))
        ip_data['open_ports_scanners'] = ports_scanners

        all_ports = list(integrated_ports) + list(ports_scanners.keys())
        self._obtain_ports_info(all_ports, ip_data)

        return ip_data


class PortToService:
    """
    Class used for translating port numbers into information about running service(s). Class uses static services.csv
    translation table and whatportis package (uses IANA port database), when no record found in the translation table.
    """

    SERVICES_PATH = get_modules_data_path().joinpath("services.csv")

    def __init__(self):
        self.services = {}
        # load static translation file (service.csv)
        with self.SERVICES_PATH.open() as services_f:
            loaded_services = csv.DictReader(services_f)
            for line in loaded_services:
                self.services[line['port_number']] = {
                    'labels': [line['label']] if "-" not in line['label'] else line['label'].split('-'),
                    'service': line['application'],
                    'description': line['description']
                }

    @staticmethod
    def _get_service_from_iana(port_number: int) -> list:
        """
        Get running services name based on open port number by get_ports(), which queries port number to IANA database
        """
        services_info = get_ports(str(port_number))
        services = []
        names, descriptions = set(), set()
        for service in services_info:
            # this check is necessary for ports like 80, where there is 4 different names of service
            # (www, www-http, http) with same description, but nobody wants all these three names for one service
            if service.name in names or service.description in descriptions:
                continue

            names.add(service.name)
            descriptions.add(service.description)
            services.append({
                'service': service.name,
                'description': service.description
            })

        return services

    def get_port_info(self, port_number: int) -> list:
        """
        Return information about services running on certain port. First try to find data in static services.csv file,
        if port not found, use IANA database.
        :param port_number: port number to query
        :return: list of services, that uses such port
        """
        try:
            return [copy(self.services[str(port_number)])]
        except KeyError:
            return self._get_service_from_iana(port_number)
