from datetime import datetime, timedelta

from .integrator_base import IntegratorBase
from common.constants import TIME_STR_FORMAT


class OperatingSystem(IntegratorBase):
    """
    Integrator, which integrates information about operating system obtained from Shodan and Censys.
    """
    MAX_AGE = timedelta(days=7)
    USED_DATA_SOURCES = {"shodan", "censys"}

    def __init__(self):
        super().__init__()

    def _remove_error_data_sources(self, ip_data: dict) -> dict:
        """
        Removes data sources, which contain any error.
        :param ip_data: information about currently processed IP address
        :return: updated info about IP address
        """
        usable_ip_data = ip_data.copy()
        for source in self.available_data_sources:
            if ip_data[source].get('error') is not None:
                usable_ip_data.pop(source)
        return usable_ip_data

    def _remove_outdated_data_sources(self, ip_data: dict) -> dict:
        """
        Removes data source, which data is outdated.
        :param ip_data: information about currently processed IP address
        :return: Updated information about IP address
        """
        usable_ip_data = ip_data.copy()
        for source in self.available_data_sources:
            try:
                if ip_data[source]['timestamp'] < (datetime.now() - timedelta(days=7)).strftime(TIME_STR_FORMAT):
                    usable_ip_data.pop(source)
            except KeyError:
                pass
        return usable_ip_data

    def run_integration(self, ip: str, ip_data: dict) -> dict:
        """
        Integrates information about operating system from Shodan and Censys.
        :param ip: currently processed IP address
        :param ip_data: information about currently processed IP address
        :return: returns integrated data
        """
        usable_ip_data = self._remove_error_data_sources(ip_data)
        usable_ip_data = self._remove_outdated_data_sources(usable_ip_data)
        os = None
        if "censys" in usable_ip_data.keys():
            os = usable_ip_data.get('censys', {}).get('metadata', {}).get('os')
        if os is None and "shodan" in usable_ip_data.keys():
            os = usable_ip_data.get('shodan', {}).get('os', "NA")
        ip_data['os'] = os.lower() if isinstance(os, str) and not os == "NA" else "NA"
        return ip_data
