from logging import getLogger

from .integrator_base import IntegratorBase
from common import Configuration


class Cleaner(IntegratorBase):
    """
    Integrator which removes keys from ip_data. It can be used for final cleanup of helpful objects or data sources.
    """
    USED_DATA_SOURCES = set()
    logger = getLogger("CleanerIntegrator")

    def __init__(self):
        super().__init__()
        self.keys_to_clean = Configuration().get_config_subset("enrichment.integrators.cleaner.keys_to_clean", list)

    def run_integration(self, ip: str, ip_data: dict) -> dict:
        self.logger.debug(f"Processing ip: {ip}")
        for key in self.keys_to_clean:
            ip_data.pop(key, None)

        return ip_data
