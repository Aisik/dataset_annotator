from abc import ABC, abstractmethod
from typing import Iterable

from common import Configuration


class IntegratorBase(ABC):
    """
    Base class for every integrator, serves for integrating multiple data sources into one or just changing structure
    of ip_data.
    """
    USED_DATA_SOURCES: Iterable

    def __init__(self):
        self.conf = Configuration().config['enrichment']
        self.enabled_data_sources = self.conf['enabled_data_sources']
        # means available data sources, which are being used by an integrator
        self.available_data_sources = self._get_available_data_sources()

    def _is_data_source_available(self, data_source_name: str) -> bool:
        """
        Check if data source used in integration is enabled in configuration of data sources
        """
        return True if data_source_name in self.enabled_data_sources else False

    def _get_available_data_sources(self) -> set:
        """
        Gets all data sources name, which are being used in port integration and are enabled in data sources
        configuration.
        :return: List of all data sources, which can be used in integration
        """
        available_sources = set()
        for data_source in self.USED_DATA_SOURCES:
            if self._is_data_source_available(data_source):
                available_sources.add(data_source)
        return available_sources

    def close(self) -> None:
        """
        This method runs, when all data are integrated and should be used as finalization method (destructor)
        :return: None
        """
        pass

    @abstractmethod
    def run_integration(self, ip: str, ip_data: dict) -> dict:
        """
        This is the integration method, which is being called by enrichment chain and is entry point of every
        integration module.
        :param ip: Currently processed IP address
        :param ip_data: information about currently processed IP address
        :return: Every integrator should returned updated ip_data (updated with its integration)
        """
        pass
