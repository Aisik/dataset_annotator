import sys
from typing import Tuple
from logging import getLogger
from datetime import datetime, timedelta

from common.constants import TIME_STR_FORMAT
from .integrator_base import IntegratorBase


class Applications(IntegratorBase):
    """
    Integrator, which integrates application info from Shodan and Censys data sources.
    """
    USED_DATA_SOURCES = {"shodan", "censys"}
    MAX_AGE = timedelta(days=7)
    logger = getLogger("ApplicationsIntegrator")

    def __init__(self):
        super().__init__()
        enabled_integrators: list = self.conf['enabled_integrators']

        # check prerequisites for applications integrator (open_ports integrator)
        try:
            open_ports_index = enabled_integrators.index("open_ports")
            applications_index = enabled_integrators.index("applications")
            if open_ports_index > applications_index:
                raise ValueError
        except ValueError:
            self.logger.error(
                "Integrator 'applications' depends on 'open_ports' integrator and 'open_ports' integrator must be run"
                " before 'applications' integrator!")
            sys.exit(2)

        self.include_banner = self.conf['integrators']['applications']['include_banner']

        # define used attributes, which will be used in final integration
        self.used_attribs = {"name", "version"}
        if self.include_banner:
            self.used_attribs.add("banner")

    def _get_usable_data_sources(self, ip_data: dict) -> dict:
        """
        Check if data from enabled data sources is not too old.
        :param ip_data: currently processed IP's data
        :return: set of usable data sources
        """
        usable_ip_data = ip_data.copy()
        for source_name in self.available_data_sources:
            if ip_data[source_name].get('error') is not None:
                continue
            if source_name == "shodan":
                ports_to_del = []
                for port_num in ip_data['shodan']['applications'].keys():
                    app_info = ip_data['shodan']['applications'][port_num]
                    if app_info['timestamp'] < (datetime.now() - self.MAX_AGE).strftime(TIME_STR_FORMAT):
                        ports_to_del.append(port_num)
                # remove old data (indexes) - list of indexes have to be reversed to prevent throw off subsequent
                # indexes
                for key in ports_to_del:
                    usable_ip_data['shodan']['applications'].pop(key)
            if ip_data.get(source_name, {}).get('timestamp', "0") < (datetime.now() - self.MAX_AGE).strftime(TIME_STR_FORMAT):
                usable_ip_data.pop(source_name)
        return usable_ip_data

    def _get_sorted_usable_sources_by_timestamp(self, ip_data: dict) -> Tuple[list, dict]:
        """
        Sorts available data sources by timestamp (age of records). Youngest is first, ...
        :param ip_data: data about processed IP, including timestamps of data sources
        :return: list of sorted data sources
        """
        usable_ip_data = self._get_usable_data_sources(ip_data)
        sorted_data_sources = []
        if "censys" in usable_ip_data.keys() and "shodan" in usable_ip_data.keys() and\
                ip_data['shodan'].get('error') is None and ip_data['censys'].get('error') is None:
            if ip_data['shodan']['timestamp'] > ip_data['censys']['timestamp']:
                sorted_data_sources = ["shodan", "censys"]
            else:
                sorted_data_sources = ["censys", "shodan"]
        else:
            # there is only once source or at least one of the sources contains error
            if "shodan" in usable_ip_data.keys() and ip_data['shodan'].get('error') is None:
                sorted_data_sources = ["shodan"]
            elif "censys" in usable_ip_data.keys() and ip_data['censys'].get('error') is None:
                sorted_data_sources = ["censys"]
        return sorted_data_sources, usable_ip_data

    def _process_app_info(self, port: int, port_info: dict, applications_by_port: dict) -> None:
        """
        Create new record about app under port key, or updated existing info about application.
        :param port: processed port number
        :param port_info: info about applications on current port number
        :param applications_by_port: dictionary containing app info under port number keys
        :return: None, port info is updated directly through applications_by_port dict
        """
        if port not in applications_by_port.keys():
            # if info about port is new, just copy it to applications_by_port dict
            applications_by_port[port] = {}
            for attr in self.used_attribs:
                applications_by_port[port][attr] = port_info.get(attr, "NA")
        else:
            for key in self.used_attribs:
                # if port info already filled by other source, update only keys, which values are empty, or NA
                if applications_by_port[port][key] in ("", "NA") or applications_by_port[port].get(key) is None:
                    applications_by_port[port][key] = port_info.get(key, "NA")

    def _integrate_app_infos(self, ip_data: dict, sorted_data_sources) -> dict:
        """
        Integrates info about applications from all usable data sources.
        :param ip_data: data about processed IP including info from data sources
        :param sorted_data_sources: sorted usable data sources
        :return: integrated app information in dict (by port keys)
        """
        applications_by_port = {}
        for data_source in sorted_data_sources:
            for port, info in ip_data[data_source]['applications'].items():
                if port in ip_data['open_ports'] or port in ip_data['open_ports_scanners'].keys():
                    self._process_app_info(port, info, applications_by_port)
        return applications_by_port

    @staticmethod
    def remove_all_NA_values(port_info: dict) -> dict:
        """
        Removes all empty or NA values from dict.
        :param port_info: dict to update
        :return: cleaned dict
        """
        updated_port_info = port_info.copy()
        for key, value in port_info.items():
            if value in ("NA", ""):
                updated_port_info.pop(key)
        return updated_port_info

    @classmethod
    def _get_clean_app_infos(cls, apps_by_port: dict) -> list:
        """
        Removes all empty app info dicts (or dict containing NA values) and cancel indexing by port, just use list
        :param apps_by_port: app infos
        :return: updated list of applications
        """
        applications = []
        for _, app_info in apps_by_port.items():
            clean_info = cls.remove_all_NA_values(app_info)
            if clean_info:
                applications.append(clean_info)
        # this list comprehensions removes completely duplicate applications
        return [dict(app_items_tuple) for app_items_tuple in {tuple(app.items()) for app in applications}]

    def run_integration(self, ip: str, ip_data: dict) -> dict:
        """
        Goes through every available data source's application info and integrates it together
        :param ip: currently processed IP
        :param ip_data: data from data sources
        :return: integrated and updated data from data sources
        """
        self.logger.debug(f"Processing ip: {ip}!")

        sorted_sources, usable_ip_data = self._get_sorted_usable_sources_by_timestamp(ip_data)
        apps_by_port = self._integrate_app_infos(usable_ip_data, sorted_sources)

        ip_data['applications'] = self._get_clean_app_infos(apps_by_port)
        return ip_data
