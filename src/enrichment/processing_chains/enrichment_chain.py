import inspect
from abc import ABC, abstractmethod
from importlib import import_module
from logging import Logger
from pathlib import Path

import sys

from common.utils import dict_enrich


class EnrichmentChain(ABC):
    """
    Base class for every enrichment chain in dataset annotator. For example annotator can use multiple data sources and
    these should be all called with enrichment chain from one spot for easy understanding, implementation and
    maintainability.
    """
    PATH_TO_MODULES: Path
    logger: Logger
    BASE_CLASS = None
    CHAIN_METHOD_NAME: str

    def __init__(self, enabled_modules: list):
        self.ip_data = {}

        self._post_init()

        available_modules = self._get_available_modules()
        self._check_enabled_modules_availability(enabled_modules, available_modules)
        imported_modules = self._import_enabled_modules(enabled_modules)
        self.enrichment_objects = self._init_enabled_modules(imported_modules)

    @abstractmethod
    def _post_init(self):
        """
        In this method should child class initialize following class variables:
            mandatory:
                - PATH_TO_MODULES - representing path to modules, which will be used in chain
                - BASE_CLASS - class, which from which have to derive all modules of chain
                - logger - just init logger with name of child class
            arbitrary:
                - CHAIN_METHOD_NAME - if specific enrichment chain just wants to run all chain's module
                without additional configuration, it can specify just name of chain's modules entry method name
        :return: None
        """
        pass

    @classmethod
    def _get_available_modules(cls) -> list:
        """
        Finds all available data_sources in enrichment package.
        :return: List of available data_sources.
        """
        available_modules = []
        for module in cls.PATH_TO_MODULES.glob("*"):
            if module.is_file() and module.suffix == ".py":
                # save module name without ".py" suffix
                available_modules.append(module.stem)
            elif module.is_dir() and "__init__.py" in module.glob("*"):
                available_modules.append(module.name)
        return available_modules

    def _check_enabled_modules_availability(self, enabled_modules: list, available_modules: list) -> None:
        """
        Check if all enabled data_sources from configuration really exist in enrichment package.
        :param enabled_modules: List of enabled data_sources from configuration.
        :param available_modules: List of all available data_sources in enrichment package
        :return: None
        """
        missing_modules = set(enabled_modules) - set(available_modules)
        if missing_modules:
            self.logger.error(f"Some of enabled enrichment data_sources are not available (not in data_sources folder), "
                              f"specifically: {missing_modules}")
            sys.exit(2)

    def _import_enabled_modules(self, enabled_modules: list) -> list:
        """
        Import all enabled data_sources.
        :param enabled_modules: List of all enabled data_sources
        :return: List of imported enabled data_sources (instances of module objects).
        """
        self.logger.debug("Importing modules...")

        # BASE_CLASS.__module__ gives modules whole path "enrichment.xxx.BASE_CLASS", so just remove part from last
        # dot (".BASE_CLASS")
        import_path_prefix = self.BASE_CLASS.__module__[:(self.BASE_CLASS.__module__.rfind('.') + 1)]
        enabled_modules = [import_path_prefix + name for name in enabled_modules]
        imported_modules = [import_module(module) for module in enabled_modules]

        # [len(import_path_prefix):] removes "enrichment.xxx" prefix of every imported module name
        self.logger.debug(f"Imported modules: "
                          f"{[module.__name__[len(import_path_prefix):] for module in imported_modules]}")
        return imported_modules

    @classmethod
    def _init_enabled_modules(cls, imported_modules: list) -> list:
        """
        Find all enrichment classed in enabled data_sources and create their instances (initialize them). These objects
        can be then used to run get_data_for_ip() method in chain one by one.
        :param imported_modules: list of module's file instance.
        :return: List of initialized enrichment objects.
        """
        enrichment_callables = []
        for module_name in imported_modules:
            for _, obj in inspect.getmembers(module_name):
                # every enrichment module has to derive from Enrichment base!
                if inspect.isclass(obj) and cls.BASE_CLASS in obj.__bases__:
                    enrichment_callables.append(obj())
        return enrichment_callables

    @abstractmethod
    def run(self, ip: str, flush_data=True, **kwargs) -> dict:
        """
        Every chain should be called with run() method, which runs all modules in the chain.
        :param ip: IP address which will be processed by the chain
        :param flush_data: Flag, which clears instance's ip_data variable. Generally should always be true, but there
                           may be some cases, where this is not desired.
        :param kwargs: additional arguments of specific enrichment chain
        :return: processed ip_data by all chain's modules
        """
        self.logger.info(f"***** Starting processing ip: {ip} *****")

        for enrichment_obj in self.enrichment_objects:
            chain_method = getattr(enrichment_obj, self.CHAIN_METHOD_NAME)
            # generally module in chain should be able to update (enrich) ip_data structure, but not create entirely
            # new one or rewrite already existing values, because it could break code of some other chain (integrators)
            # if completely updating ip_data structure is desired, then use integrator or create new processing chain
            dict_enrich(self.ip_data, chain_method(ip, self.ip_data.get('hostname', "")))

        ip_data = self.ip_data
        if flush_data:
            self.ip_data = {}

        return ip_data

    def close(self) -> None:
        """
        Should be called, when the chain is no longer needed (should be used as destructor, but has to be called
        explicitly, because python's __del__() method does not guarantee calling itself when the interpreter exists.
        https://docs.python.org/3/reference/datamodel.html#object.__del__
        :return: None
        """
        self.logger.info("Closing chain and its objects!")
        for obj in self.enrichment_objects:
            obj.close()

