from typing import Dict
from copy import deepcopy
import time
from logging import getLogger

from common import Configuration

from .integrators_chain import IntegratorsChain
from .data_sources_chain import DataSourcesChain

logger = getLogger("Enrichment runner")


class StopAndSaveData(Exception):
    """
    This exception can be used, when some big error occurres in data_source or integrator processing and user wants to
    save so far processed data and not just stop the program. Then raise this exception and enrichment chain will stop
    processing ips and will save the results, which it got so far.
    """
    pass


def run_enrichment_on_hosts_list(hosts_to_process: Dict[str, dict], data_sources_chain: DataSourcesChain,
                                 integrators_chain: IntegratorsChain) -> None:
    """
    Process dict of host structures, which means run data_sources chain and integrators chain on every ip address in
    host dictionary.
    :param hosts_to_process: dictionary containing IP under key and basic dict struct
    :param data_sources_chain: initialized chain of data sources
    :param integrators_chain: initialized chain of integrators
    :return: None, hosts are updated directly through hosts_to_process argument
    """
    try:
        for ip in hosts_to_process.keys():
            hosts_to_process[ip].update(data_sources_chain.run(ip))
            hosts_to_process[ip] = integrators_chain.run(ip, ip_raw_data=deepcopy(hosts_to_process[ip]))
    except Exception as e:
        logger.error(e)
        logger.info("Some module from enrichment chain raised StopAndSaveData exception, annotator stops processing and"
                    " saves so far processed data!")
        pass


def run_enrichment(hosts: dict):
    """
    Initializes all enrichment chains and runs them on passed hosts.
    :param hosts: input hosts structures
    :return: None, hosts are updated directly through its dict
    """
    conf = Configuration()

    data_sources_chain = DataSourcesChain(conf['enrichment']['enabled_data_sources'])
    integrators_chain = IntegratorsChain(conf['enrichment']['enabled_integrators'])
    enrichment_start = time.time()
    total_host_count = 0
    if conf['input']['type'] == "identifier_list":
        for f_name, host_structures in hosts.items():
            total_host_count += len(host_structures.keys())
            run_enrichment_on_hosts_list(host_structures, data_sources_chain, integrators_chain)
    else:
        # input_type is 'captured_traffic'
        total_host_count += len(hosts.keys())
        run_enrichment_on_hosts_list(hosts, data_sources_chain, integrators_chain)

    total_enrich_time = time.time() - enrichment_start
    logger.info(f"Total host count is {total_host_count}.")
    if total_host_count != 0:
        logger.info(f"Total enrichment time: {total_enrich_time}. Average "
                    f"time per host is {total_enrich_time / total_host_count}")
    integrators_chain.close()
    data_sources_chain.close()
