from pathlib import Path
import logging

from .enrichment_chain import EnrichmentChain
from enrichment.data_sources import DataSourceBase


class DataSourcesChain(EnrichmentChain):
    """
    Enrichment chain, which handles running of all enabled data sources.
    """
    def __init__(self, enabled_modules: list):
        super().__init__(enabled_modules)

    def _post_init(self):
        """
        Initializes needed class variables of enrichment chain.
        :return:
        """
        self.__class__.PATH_TO_MODULES = Path(__file__).parent.parent.joinpath("data_sources")
        self.__class__.logger = logging.getLogger("DataSourcesChain")
        self.__class__.BASE_CLASS = DataSourceBase
        self.__class__.CHAIN_METHOD_NAME = "get_data_for_ip"

    def run(self, ip: str, flush_data=True, **kwargs) -> dict:
        """
        Since the chain method name is specified in post_init, just call base run() method.
        :param ip: currently processed IP
        :param flush_data: clear chains ip_data structure after all modules are called
        :param kwargs: additional args
        :return: data processed by all chain's modules
        """
        return super().run(ip, flush_data)


