from pathlib import Path
import logging

from .enrichment_chain import EnrichmentChain
from enrichment.integrators import IntegratorBase


class IntegratorsChain(EnrichmentChain):
    def __init__(self, enabled_modules: list):
        super().__init__(enabled_modules)

    def _post_init(self):
        """
        Initialize all chains needed class variables.
        :return:
        """
        self.__class__.PATH_TO_MODULES = Path(__file__).parent.parent.joinpath("integrators")
        self.__class__.logger = logging.getLogger("IntegratorsChain")
        self.__class__.BASE_CLASS = IntegratorBase

    def run(self, ip: str, flush_data=True, **kwargs) -> dict:
        """
        Runs chain of integrators. Do not forget to pass ip_raw_data attribute in kwargs
        :param ip:
        :param flush_data:
        :param kwargs:
        :return:
        """
        self.logger.info(f"***** Starting processing ip: {ip} *****")

        assert kwargs.get('ip_raw_data') is not None, "IntegratorsChain run() method needs to pass ip_raw_data arg!"

        for enrichment_obj in self.enrichment_objects:
            self.ip_data = enrichment_obj.run_integration(ip, kwargs['ip_raw_data'])

        ip_data = self.ip_data
        if flush_data:
            self.ip_data = {}

        return ip_data



