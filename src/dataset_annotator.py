#!/usr/bin/env python3
import argparse
import json
import logging
from logging import Logger
from pathlib import Path
import sys

from common import load_configuration
from enrichment.processing_chains.runner import run_enrichment
from input_processing.loader import InputLoader


def get_parsed_args():
    """
    Initialize ArgumentParser for command line arguments.
    :return: initialized ArgumentParser
    """
    parser = argparse.ArgumentParser(
        prog="dataset_annotator.py",
        description="Datataset anototar laods configuration file from args and from that is loaded path to input "
                    "identifiers, which will be used for annotation. On output user gets json file with anotated "
                    "identifiers."
    )
    optional_args = parser.add_argument_group("optional arguments")
    optional_args.add_argument("-c", "--config", metavar="FILENAME", default="../configuration/conf.yml",
                               help="Path to configuration file (default: conf.yml)")
    optional_args.add_argument("-v", "--verbose", action="store_true", help="Turn on debug mode of logging module")
    return parser.parse_args()


def set_logging_level_for_libs(list_of_libs: list, logging_level) -> None:
    """
    Sets logging level of certain libraries.
    :param list_of_libs: list of libraries, which logging level will be modified
    :param logging_level: new logging level
    :return: None
    """
    for lib in list_of_libs:
        logging.getLogger(lib).setLevel(logging_level)


def setup_logging(args_verbose: False) -> Logger:
    """
    Setups logging configuration of anotator.
    :param args_verbose: flag, which defines, if debugging level should be debug or not
    :return: initialized Logger object
    """
    LOG_FORMAT = "%(asctime)-15s,%(name)s [%(levelname)s] %(message)s"
    LOG_DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"
    logging_level = logging.INFO
    if args_verbose:
        logging_level = logging.DEBUG
    logging.basicConfig(level=logging_level, format=LOG_FORMAT, datefmt=LOG_DATE_FORMAT,
                        handlers=[logging.FileHandler('../out.log', 'w'), logging.StreamHandler(sys.stdout)])
    logger = logging.getLogger("dataset_annotator.py")
    set_logging_level_for_libs(["urllib3", "ipwhois"], logging.WARNING)
    return logger


if __name__ == '__main__':
    args = get_parsed_args()

    logger = setup_logging(args.verbose)
    config = load_configuration(args.config)

    input_config = config['input']
    # load all hosts
    input_loader = InputLoader(Path(input_config['path']), input_config['type'])
    hosts = input_loader.load_input_ips()

    run_enrichment(hosts)

    # save the result
    output_path_dir = Path(config['output']['path_dir'])
    if input_config['type'] == "identifier_list":
        for file_name in hosts.keys():
            end_filename = "annotation_" + file_name[:-4]
            logger.info(f"Writing annotation results into {end_filename + '.json'}")
            output_path = output_path_dir.joinpath(end_filename).with_suffix(".json")
            with output_path.open('w') as output_f:
                json.dump(hosts[file_name], output_f, sort_keys=True, indent=4)
    else:
        # input_type is 'captured_traffic'
        end_filename = "data_annotation.json"
        logger.info(f"Writing annotation results into {end_filename}")
        output_path = output_path_dir.joinpath(end_filename).with_suffix(".json")
        with output_path.open('w') as output_f:
            json.dump(hosts, output_f, sort_keys=True, indent=4)
