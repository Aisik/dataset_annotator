import pytest
from datetime import datetime, timedelta

from enrichment.integrators.operating_system import OperatingSystem
from common.constants import TIME_STR_FORMAT


@pytest.fixture(scope="module")
def os() -> OperatingSystem:
    return OperatingSystem()


@pytest.fixture()
def input_data() -> dict:
    return {
        'shodan': {'timestamp': (datetime.now() - timedelta(days=2, hours=5)).strftime(TIME_STR_FORMAT),
                   'os': None},
        'censys': {'timestamp': (datetime.now() - timedelta(days=1)).strftime(TIME_STR_FORMAT),
                   'metadata': {'os': "Ubuntu"}}
    }


def test_run_integration(os: OperatingSystem, input_data: dict):
    expected_os = "ubuntu"
    calculated_os = os.run_integration("", input_data)['os']
    assert expected_os == calculated_os

    input_data['censys'].pop('metadata')
    calculated_os = os.run_integration("", input_data)['os']
    assert "NA" == calculated_os

    input_data['shodan']['os'] = "ubuntu"
    calculated_os = os.run_integration("", input_data)['os']
    assert "ubuntu" == calculated_os

    input_data['shodan']['timestamp'] = (datetime.now() - (OperatingSystem.MAX_AGE + timedelta(days=4))).\
        strftime(TIME_STR_FORMAT)
    calculated_os = os.run_integration("", input_data)['os']
    assert "NA" == calculated_os
