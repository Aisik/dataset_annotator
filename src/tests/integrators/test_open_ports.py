import pytest
from datetime import datetime, timedelta

from common.constants import TIME_STR_FORMAT
from enrichment.integrators.open_ports import OpenPorts


@pytest.fixture(scope="module")
def open_ports() -> OpenPorts:
    return OpenPorts()


@pytest.fixture()
def input_adict_shodan_censys() -> dict:
    return {
        'adict': {'error': "Something went wrong!"},
        # age is now - 5 days
        'shodan': {'timestamp': (datetime.now()-timedelta(days=5)).strftime(TIME_STR_FORMAT)},
        # age is now - 8 days
        'censys': {'timestamp': (datetime.now()-timedelta(days=8)).strftime(TIME_STR_FORMAT)}
    }


def test_get_valid_data_sources(input_adict_shodan_censys: dict, open_ports: OpenPorts):
    valid_data_sources = open_ports._get_valid_data_sources(input_adict_shodan_censys)
    true_valid_data_sources = {"shodan"}
    assert valid_data_sources == true_valid_data_sources, "Only shodan is valid data source!"

    # make valid Censys data source too
    input_adict_shodan_censys['censys'] = {'timestamp': (datetime.now()-timedelta(days=3)).strftime(TIME_STR_FORMAT)}
    true_valid_data_sources.add("censys")
    valid_data_sources = open_ports._get_valid_data_sources(input_adict_shodan_censys)
    assert valid_data_sources == true_valid_data_sources, "Shodan and Censys should be valid data sources!"

    # test censys without timestamp - without timestamp is data source not valid, because age of data cannot be checked
    input_adict_shodan_censys['censys'].pop('timestamp')
    valid_data_sources = open_ports._get_valid_data_sources(input_adict_shodan_censys)
    true_valid_data_sources.remove("censys")
    assert valid_data_sources == true_valid_data_sources, "Shodan and Censys should be valid data sources!"


@pytest.fixture()
def port_input_data() -> dict:
    return {
        'adict': {'open_ports': [80, 443, 53]},
        'censys': {'open_ports': [443, 80, 22]},
        'shodan': {'open_ports': {
            443: (datetime.now() - timedelta(days=3)).strftime(TIME_STR_FORMAT),
            80: (datetime.now() - timedelta(days=5)).strftime(TIME_STR_FORMAT),
            143: (datetime.now() - timedelta(days=15)).strftime(TIME_STR_FORMAT)
        }}
    }


def test_get_united_ports(open_ports: OpenPorts, port_input_data: dict):
    expected_result = {80, 443, 53, 22}

    valid_data_sources = {"adict", "shodan", "censys"}
    united_ports = open_ports._get_united_ports(valid_data_sources, port_input_data)

    assert expected_result == united_ports, "Wrong port union, probably used port 143, which should not be used!"
    assert 143 not in port_input_data['shodan'].keys(), "Unused port should be removed!"


def test_get_integrated_ports_all_sources(open_ports: OpenPorts, port_input_data: dict):
    expected_open_ports = [80, 443, 53]
    expected_open_ports_scanners = {
        22: OpenPorts.SCANNERS_CONFIDENCE_LEVELS['low']
    }
    united_ports = [80, 443, 53, 22]
    valid_data_sources = {"shodan", "censys", "adict"}
    calculated_open_ports, calculated_ports_scanners = open_ports._get_integrated_ports(united_ports, port_input_data,
                                                                                        valid_data_sources)

    assert set(expected_open_ports) == calculated_open_ports, "Integration of ADiCT ports is wrong!"
    assert expected_open_ports_scanners == calculated_ports_scanners, "Integration of scanner ports is wrong!"

    united_ports.append(143)
    expected_open_ports_scanners[143] = OpenPorts.SCANNERS_CONFIDENCE_LEVELS['low']
    _, calculated_ports_scanners = open_ports._get_integrated_ports(united_ports, port_input_data, valid_data_sources)
    assert expected_open_ports_scanners == calculated_ports_scanners, "Port 143 should be in integrated ports!"

    port_input_data['censys']['open_ports'].append(143)
    expected_open_ports_scanners[143] = OpenPorts.SCANNERS_CONFIDENCE_LEVELS['high']
    _, calculated_ports_scanners = open_ports._get_integrated_ports(united_ports, port_input_data, valid_data_sources)
    assert expected_open_ports_scanners == calculated_ports_scanners, "Port 143 should be now with high confidence!"


def test_get_integrated_ports_adict_and_one_scanner(open_ports: OpenPorts, port_input_data: dict):
    expected_open_ports = [80, 443, 53]
    expected_open_ports_scanners = {
        143: OpenPorts.SCANNERS_CONFIDENCE_LEVELS['medium']
    }
    valid_data_sources = {"adict", "shodan"}
    united_ports = [80, 443, 53, 143]
    calculated_open_ports, calculated_ports_scanners = open_ports._get_integrated_ports(united_ports, port_input_data,
                                                                                        valid_data_sources)
    assert expected_open_ports_scanners == calculated_ports_scanners, "Port 143 should be with medium confidence!"
    assert set(expected_open_ports) == calculated_open_ports, "Integration of ADiCT ports is wrong!"

    valid_data_sources.remove("shodan")
    valid_data_sources.add("censys")
    united_ports.remove(143)
    expected_open_ports_scanners.pop(143)
    united_ports.append(22)
    expected_open_ports_scanners[22] = OpenPorts.SCANNERS_CONFIDENCE_LEVELS['medium']
    _, calculated_ports_scanners = open_ports._get_integrated_ports(united_ports, port_input_data, valid_data_sources)
    assert expected_open_ports_scanners == calculated_ports_scanners, "Port 22 should be with medium confidence!"


def test_get_integrated_ports_scanners_only(open_ports: OpenPorts, port_input_data: dict):
    port_input_data['adict']['open_ports'] = []
    expected_open_ports = set()
    expected_open_ports_scanners = {
        443: OpenPorts.SCANNERS_CONFIDENCE_LEVELS['high'],
        80: OpenPorts.SCANNERS_CONFIDENCE_LEVELS['high'],
        22: OpenPorts.SCANNERS_CONFIDENCE_LEVELS['low'],
        143: OpenPorts.SCANNERS_CONFIDENCE_LEVELS['low']
    }
    united_ports = [80, 443, 22, 143]
    valid_data_sources = {"censys", "shodan", "adict"}
    calculated_open_ports, calculated_ports_scanners = open_ports._get_integrated_ports(united_ports, port_input_data,
                                                                                        valid_data_sources)
    assert expected_open_ports_scanners == calculated_ports_scanners, "Wrong integration of scanner ports!"
    assert expected_open_ports == calculated_open_ports, "There should be no ports integrated from ADiCT!"
