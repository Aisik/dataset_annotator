import pytest
from datetime import datetime, timedelta

from enrichment.integrators.applications import Applications
from common.constants import TIME_STR_FORMAT


@pytest.fixture(scope="module")
def applications() -> Applications:
    return Applications()


@pytest.fixture()
def input_timestamps() -> dict:
    return {
        'shodan': {'timestamp': (datetime.now() - timedelta(days=2, hours=5)).strftime(TIME_STR_FORMAT),
                   'applications': {}},
        'censys': {'timestamp': (datetime.now() - timedelta(days=1)).strftime(TIME_STR_FORMAT)}
    }


@pytest.fixture()
def input_apps_shodan():
    return {
        113: {'banner': '113 , 33836 : ERROR : HIDDEN-USER\r\n', 'timestamp': '2021-02-18T23:50:18'},
        995: {'banner': '+OK ready ', 'timestamp': '2021-02-18T05:44:09'},
        465: {'name': 'Sendmail', 'version': '8.16.1/8.16.1', 'banner': 'Sendmail 8.16.1/8.16.1;',
              'timestamp': '2021-02-18T03:52:18'},
        143: {'banner': 'Dovecot ready.', 'timestamp': '2021-02-17T14:03:58'},
        79: {'banner': 'must provide username\r\n\n', 'timestamp': '2021-02-17T06:27:25'},
        111: {'banner': 'Portmap\nProgram\tVersion\tProtocol\tPort\nportmapper\t4\ttcp\t111\nportmapper\t3',
              'timestamp': '2021-02-16T08:25:33'},
        21: {'banner': 'You are not welcome to use ftpd from', 'timestamp': '2021-02-16T04:16:21'},
        53: {'banner': '', 'timestamp': '2021-02-09T09:14:22'},
        22: {'name': 'OpenSSH', 'version': '7.9', 'info': 'protocol 2.0', 'banner': 'SSH-2.0-OpenSSH_7.9',
             'timestamp': '2021-02-14T13:58:11'},
        25: {'name': 'Sendmail', 'version': '8.16.1/8.16.1', 'banner': '220 Sendmail 8.16.1/8.16.1',
             'timestamp': '2021-02-14T00:43:03'},
        587: {'name': 'Sendmail', 'version': '8.16.1/8.16.1', 'banner': '220 Sendmail 8.16.1/8.16.1',
              'timestamp': '2021-02-03T07:22:40'},
        993: {'banner': 'Dovecot ready', 'timestamp': '2021-01-29T19:33:26'}
    }


@pytest.fixture()
def input_apps_censys():
    return {
        993: {'banner': 'Dovecot ready.'},
        995: {'banner': '+OK ready'},
        587: {'banner': 'Sendmail 8.16.1/8.16.1;', 'name': 'Sendmail', 'description': 'Sendmail'},
        143: {'banner': 'Dovecot ready.', 'name': 'Dovecot', 'description': 'Dovecot'},
        465: {'banner': 'Sendmail 8.16.1/8.16.1'},
        22: {'banner': 'SSH-2.0-OpenSSH_7.9', 'name': 'OpenSSH', 'version': '7.9', 'description': 'OpenSSH 7.9'},
        25: {'banner': 'Sendmail 8.16.1/8.16.1', 'name': 'Sendmail', 'description': 'Sendmail'}
    }


@pytest.fixture()
def app_integration_result():
    return {
        993: {'name': 'NA', 'version': 'NA'},
        995: {'name': 'NA', 'version': 'NA'},
        587: {'name': 'Sendmail', 'version': 'NA'},
        143: {'name': 'Dovecot', 'version': 'NA'},
        465: {'name': 'Sendmail', 'version': '8.16.1/8.16.1'},
        22: {'name': 'OpenSSH', 'version': '7.9'},
        25: {'name': 'Sendmail', 'version': 'NA'}
    }


def test_get_usable_data_sources(applications: Applications, input_timestamps: dict, input_apps_shodan: dict):
    expected_usable_sources = {"censys", "shodan"}
    usable_sources = applications._get_usable_data_sources(input_timestamps)
    assert all(source in expected_usable_sources for source in usable_sources.keys())

    input_timestamps['censys']['timestamp'] = datetime(year=2021, month=2, day=1, hour=10, minute=11, second=20)\
        .strftime(TIME_STR_FORMAT)
    expected_usable_sources.remove("censys")
    usable_sources = applications._get_usable_data_sources(input_timestamps)
    assert expected_usable_sources == set(usable_sources.keys())


def test_get_sorted_usable_sources_by_timestamp(applications: Applications, input_timestamps: dict):
    expected_sorted_sources = ["censys", "shodan"]
    sorted_sources, _ = applications._get_sorted_usable_sources_by_timestamp(input_timestamps)
    assert expected_sorted_sources == sorted_sources

    input_timestamps['shodan']['timestamp'] = (datetime.now() - timedelta(hours=2)).strftime(TIME_STR_FORMAT)
    expected_sorted_sources = ["shodan", "censys"]
    sorted_sources, _ = applications._get_sorted_usable_sources_by_timestamp(input_timestamps)
    assert expected_sorted_sources == sorted_sources


def test_integrate_app_infos(applications: Applications, input_apps_shodan: dict, input_apps_censys: dict,
                             input_timestamps: dict, app_integration_result):
    ip_data = {
        'shodan': {'applications': input_apps_shodan},
        'censys': {'applications': input_apps_censys},
        'open_ports_scanners': {993: "", 995: "", 587: "", 143: "", 465: "", 22: "", 25: ""},
        'open_ports': []
    }
    ip_data['shodan']['timestamp'] = input_timestamps['shodan']['timestamp']
    ip_data['censys'].update(input_timestamps['censys'])
    sorted_sources, usable_ip_data = applications._get_sorted_usable_sources_by_timestamp(ip_data)
    calculated_applications = applications._integrate_app_infos(usable_ip_data, sorted_sources)
    assert app_integration_result == calculated_applications


def test_get_clean_app_infos(applications: Applications, app_integration_result: dict):
    expected_result = [
        {
            "name": "Dovecot"
        },
        {
            "name": "OpenSSH",
            "version": "7.9"
        },
        {
            "name": "Sendmail"
        },
        {
            "name": "Sendmail",
            "version": "8.16.1/8.16.1"
        }
    ]

    calculated_applications = applications._get_clean_app_infos(app_integration_result)
    a = sorted(expected_result, key=lambda app: app['name'])
    aa = a.copy()
    b = sorted(calculated_applications, key=lambda app: app['name'])
    bb = b.copy()
    for item in a:
        assert item in b
        bb.remove(item)
        aa.remove(item)
    assert len(aa) == 0
    assert len(bb) == 0
