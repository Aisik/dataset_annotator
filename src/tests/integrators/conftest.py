import pytest
from pathlib import Path
import yaml

from common import Configuration


# autouse is here just to ensure earlier call than open_ports fixture (it gives higher priority to fixture)
@pytest.fixture(scope="package", autouse=True)
def conf() -> Configuration:
    with Path(__file__).parent.joinpath("conf.yml").open() as config_f:
        config = yaml.safe_load(config_f)
    return Configuration(config)
