"""
In this file should be placed all global constants used by dataset_annotator.
"""

# general datetime format used across annotator
TIME_STR_FORMAT = "%Y-%m-%dT%H:%M:%S"
