import logging
import sys
import yaml
from pathlib import Path

logger = logging.getLogger("Configuration")


class Configuration:
    """
    Class which loads and holds anotator's configuration between all instances of this class. Represents singleton
    design pattern.
    """
    _config: dict = None

    def __init__(self, config: dict = None):
        if config is not None and self.config is None:
            assert isinstance(config, dict), "Configuration must be instance of dict!"
            self.config = config
            self.validate_config()
            logger.info("Configuration loaded!")

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, new_config):
        self.__class__._config = new_config

    def _process_composite_key(self, composite_key: str):
        """
        Processes composite key as keys joined by '.' and goes deep into config based on these keys.
        :param composite_key: nested joined keys
        :return: data placed under composite key
        """
        keys = composite_key.split(".")
        try:
            config_subset = self.config[keys[0]]
            for key in keys[1:]:
                config_subset = config_subset[key]
        except KeyError:
            logger.error(f"Composite key '{composite_key}' passed to constructor cannot be parsed!")
            sys.exit(2)
        return config_subset

    def get_config_subset(self, composite_key: str, subset_type=None):
        """
        Enables shortcut in multiple access in nested keys, user can just specify composite key, which can be used to
        return just subset of wanted config, which is set by composite_key, instead of repeating nested dict access
        through multiple keys everytime.
        Instead of calling 'conf['enrichment']['data_sources']['adict']' multiple times, user can call this method with
        composite key 'enrichment.data_sources.adict'.
        :param composite_key: Multiple dict keys in nested dict structure joined by '.' (e.g. enrichment.data_sources.adict)
        :param subset_type:  user may add additional check on configuration subset data type, if subset is not instance
        of data type, error is raised
        :return: data placed under composite key
        """
        try:
            subset = self._process_composite_key(composite_key)
        except AttributeError:
            logger.error("Composite key for accessing config subset is not set!")
            sys.exit(2)

        # user may add additional check on configuration subset data type
        if subset_type is not None:
            assert isinstance(subset, subset_type)
        return subset

    def __getitem__(self, item):
        return self._config[item]

    def __setitem__(self, key, value):
        self._config[key] = value

    def validate_config(self):
        assert self.config['input']['type'] in \
               ("identifier_list", "captured_traffic"), "Invalid input type in configuration, use either " \
                                                        "'identifier_list' or 'captured_traffic'!"


def load_configuration(path_to_config: str) -> Configuration:
    """
    Loads configuration from configuration input file.
    :param path_to_config: path to configuration file
    :return: initialized Configuration object
    """
    config_path = Path(path_to_config)
    if not config_path.exists() or not config_path.is_file():
        logger.error("Path to annotator's configuration is wrong")
        sys.exit(2)

    with config_path.open() as config_f:
        try:
            config = yaml.safe_load(config_f)
        except yaml.YAMLError as e:
            logger.error(e)
            logger.error("Error occurred while loading annotator's configuration, annotator stops now!")
            sys.exit(2)

    # init global configuration
    return Configuration(config)
