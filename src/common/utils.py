"""
Common utils used across all source file used by dataset_anotator.
"""
from pathlib import Path
from copy import copy


def find_in_nested_dict(key: str, nested_dict: dict, var_type=None):
    """
    Recursive search for key in nested dictionary.
    :param key: searched key
    :param nested_dict: dictionary to be searched
    :param var_type: filter, which defines type of value, which must be held under searched key
    :return: value under searched key
    """
    for k, v in nested_dict.items():
        if k == key:
            if var_type is not None:
                if isinstance(v, var_type):
                    return v
            else:
                return v
        if isinstance(v, dict):
            res = find_in_nested_dict(key, v)
            if res is not None:
                return res


def get_project_root_path() -> Path:
    return Path(__file__).parent.parent.parent


def get_modules_data_path() -> Path:
    return get_project_root_path().joinpath("data")


def remove_all_NA_values(port_info: dict) -> dict:
    """
    Removes all empty or NA values from dict.
    :param port_info: dict to update
    :return: cleaned dict
    """
    updated_port_info = copy(port_info)
    for key, value in port_info.items():
        if value in ("NA", ""):
            updated_port_info.pop(key)
    return updated_port_info


def dict_enrich(dict_to_enrich: dict, new_dict: dict, recursive=True) -> None:
    """
    Enriches each value of dictionary:
        - if key does not exist, just copy value
        - if key exists and it is list, add value(s) to that list (if the value does not yet exist)
        - if key exists and it is dict, recursively call this function
    :param dict_to_enrich: original dict, which will be enriched
    :param new_dict: dict with new data
    :param recursive: turns on recursive updating of each dict, if False, no dictionary will be updated
    :return: None, data gets updated directly through dict_to_enrich
    """
    for new_k, new_v in new_dict.items():
        if new_k in dict_to_enrich:
            # if key exists in original dict, try to update its value
            if isinstance(dict_to_enrich[new_k], list):
                # if value is list, check if new val is also list (if yes, append every non existing value, else just
                # append value)
                if isinstance(new_v, list):
                    for list_item in new_v:
                        if list_item not in dict_to_enrich[new_k]:
                            dict_to_enrich[new_k].append(list_item)
                else:
                    if new_v not in dict_to_enrich[new_k]:
                        dict_to_enrich[new_k].append(new_v)
            elif isinstance(dict_to_enrich[new_k], dict):
                # recursively update every nested dict
                if recursive:
                    dict_enrich(dict_to_enrich[new_k], new_dict[new_k])
        else:
            dict_to_enrich[new_k] = new_v
