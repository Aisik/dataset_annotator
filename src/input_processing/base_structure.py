class BaseStructure:
    """
    Class representing dict structure, which will be used as input into enrichment data_sources. For now in default, the
    structure contains only hostname, but this design should ensure easy extensibility for new default attributes in
    the future. Just add new attrib inside __init__ and adjust structure in struct() property accordingly.
    """
    def __init__(self, hostname):
        self.hostname = hostname

    @property
    def structure(self):
        return {
            'hostname': self.hostname
        }



