import re
from socket import gethostbyaddr, gethostbyname, herror, gaierror
from typing import Tuple, Dict
from ipaddress import IPv4Address, IPv4Network
from abc import ABC, abstractmethod
import logging

from .base_structure import BaseStructure

logger = logging.getLogger("Identifier")


class UnknownInputIdentifier(Exception):
    pass


def create_input_structure_from_ip(ip_addr: str) -> dict:
    """
    Try to find hostname of certain IP and return basic structure
    :param ip_addr: IP address which will be translated into hostname
    :return: input structure for enrichment data_sources
    """
    try:
        hostname = gethostbyaddr(ip_addr)[0]
    except herror:
        hostname = "NA"
    input_structure = BaseStructure(hostname)
    return input_structure.structure


class Identifier(ABC):
    """
    Abstract Identifier class, which serves as Chain of responsibility design pattern. Every possible type of input
    identifier should inherit from this class and implement its custom _parse() method, where it tries to parse an input
    identifier.
    """
    def __init__(self, next_identifier=None):
        self.successor = next_identifier

    def parse(self, identifier) -> Dict[str, dict]:
        try:
            return self._parse(identifier)
        except Exception:
            if self.successor is not None:
                return self.successor.parse(identifier)
        return {}

    @abstractmethod
    def _parse(self, identifier) -> Dict[str, dict]:
        pass


class IPv4Addr(Identifier):
    """
    Identifier class representing IPv4 address.
    """
    def _parse(self, identifier) -> Dict[str, dict]:
        IPv4Address(identifier)
        return {
            str(identifier): create_input_structure_from_ip(identifier)
        }


class IPv4Net(Identifier):
    """
    Identifier class representing IPv4 network in CIDR notation.
    """
    def _parse(self, identifier) -> Dict[str, dict]:
        network = IPv4Network(identifier)
        all_network_records = {}
        for ip in network:
            logger.debug(f"Processing {ip}")
            all_network_records[str(ip)] = create_input_structure_from_ip(str(ip))
        return all_network_records


class Hostname(Identifier):
    """
    Identifier class representing hostname processing.
    """
    RE_HOSTNAME_VALIDATION = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)

    @classmethod
    def is_valid_hostname(cls, hostname: str) -> bool:
        """
        Checks hostname validity part by part.
        """
        if len(hostname) > 255:
            return False
        if hostname[-1] == ".":
            hostname = hostname[:-1]  # strip exactly one dot from the right, if present
        return all(cls.RE_HOSTNAME_VALIDATION.match(x) for x in hostname.split("."))

    @staticmethod
    def _create_input_structure(hostname: str) -> Tuple[str, dict]:
        """
        Creates default input structure with hostname.
        """
        try:
            ip_addr = gethostbyname(hostname)
        except gaierror:
            # error in hostname resolution
            return "", {}
        input_structure = BaseStructure(hostname)
        return ip_addr, input_structure.structure

    def _parse(self, identifier) -> Dict[str, dict]:
        """
        Try to parse identifier as hostname
        :param identifier: unknown input identifier
        :return: default anotator input structure
        """
        if self.is_valid_hostname(identifier):
            ip, structure = self._create_input_structure(identifier)
            if ip:
                return {
                    ip: structure
                }
        raise UnknownInputIdentifier
