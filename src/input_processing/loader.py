from pathlib import Path
import sys
from typing import Dict
import logging

from .parser import ParserFactory


class InputLoader:
    logger = logging.getLogger("InputLoader")
    SUPPORTED_INPUT_TYPES = {
        'identifier_list': (".txt", ),
        'captured_traffic': (".csv", ".pcap")
    }

    def __init__(self, input_path: Path, input_type: str):
        if not input_path.exists():
            self.logger.error(f"Input file {str(input_path)} does not exist!")
            sys.exit(2)

        self.input_path = input_path
        self.input_type = input_type

    def _is_supported_input(self, input_path: Path) -> bool:
        return True if input_path.suffix in self.SUPPORTED_INPUT_TYPES[self.input_type] else False

    def load_input_ips(self) -> Dict[str, dict]:
        all_records = {}
        if self.input_path.is_dir():
            for input_f_path in self.input_path.glob("*"):
                if self._is_supported_input(input_f_path):
                    all_records.update(ParserFactory.get_records_from_file(input_f_path, self.input_type))
        elif self.input_path.is_file() and self._is_supported_input(self.input_path):
            all_records.update(ParserFactory.get_records_from_file(self.input_path, self.input_type))

        return all_records




