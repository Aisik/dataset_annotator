import csv
from abc import ABC, abstractmethod
from ipaddress import IPv4Address, IPv4Network, AddressValueError
from logging import getLogger
from pathlib import Path
from typing import List

import sys
from scapy.all import rdpcap
from scapy.layers.inet import IP

from common import Configuration
from .input_identifier import IPv4Addr, Hostname, IPv4Net


class ParserFactory:
    """
    Factory used for calling the right parser based on input_type from configuration and file suffix of currently
    processed file.
    """
    logger = getLogger("ParserFactory")

    @classmethod
    def get_records_from_file(cls, path_to_file: Path, input_type: str):
        """
        Calls the right parser for input file processing.
        :param path_to_file: path to currently processed input file
        :param input_type: type of input (identifier_list|captured_traffic)
        :return: base structure for enrichment processing
        """
        cls.logger.debug(f"Processing input file {str(path_to_file)}!")
        if input_type == "identifier_list":
            new_records = IdentifierListParser().parse_file(path_to_file)
        else:
            # parse records from captured traffic
            if path_to_file.suffix == ".csv":
                new_records = CSVTrafficParser().parse_file(path_to_file)
            elif path_to_file.suffix == ".pcap":
                new_records = PCAPTrafficParser().parse_file(path_to_file)
            else:
                new_records = []

        if not new_records:
            cls.logger.warning(f"Input file {str(path_to_file.name)} is empty or contains comments only!")

        return new_records


class InputParser(ABC):
    """
    Parser used for parsing any file like input.
    """
    logger = getLogger("InputParser")

    @staticmethod
    def parse_out_comments(lines: List[str], ignore: list = None) -> List[str]:
        """
        Removes comments from input (all lines).
        :param lines: lines of blacklist
        :param ignore: list of ignored chars (for example in hostname you want to allow '#'
        :return: cleaned lines

        :raise: TypeError if ignore arg is used and is not iterable
        """
        clean_lines = []

        comment_signs = {"#", "//"}
        if ignore is not None:
            for ignored_char in ignore:
                comment_signs.remove(ignored_char)

        for line in lines:
            if not line.strip():
                continue
            if line.startswith("#") or line.startswith("//"):
                continue
            if any(comment_sign in line for comment_sign in ("#", "//")):
                identifier = line.split('#')
                if len(identifier) == 1:
                    identifier = line.split("//")
                clean_lines.append(identifier[0].strip())
                continue
            clean_lines.append(line.strip())
        return clean_lines

    @abstractmethod
    def parse_file(self, path_to_file: Path) -> dict:
        pass


class TrafficInputParser(InputParser):
    """
    Parser used for parsing all IP addresses from captured traffic a init the basic anotator structure.
    """

    def __init__(self):
        self.filter_accepted_addresses = set()
        conf = Configuration().get_config_subset('input')

        # get list of all accepted IP addresses from file defined in configuration
        try:
            with open(conf['filter_path']) as filter_f:
                lines = filter_f.readlines()
                clean_lines = self.parse_out_comments(lines)
                self.filter_accepted_addresses = self._get_all_filter_addresses(clean_lines)
        except FileNotFoundError:
            self.logger.warning("Cannot open filter file for captured_traffic input, because file does not exist!")
            self.filter_accepted_addresses = None

    @classmethod
    def _get_all_filter_addresses(cls, lines: list) -> set:
        """
        Creates list of all ip addresses, which will be used for filtering relevant IPs.
        :param lines: all lines of input filter
        :return: list of parsed filtering IP addresses
        """
        addr_filter = set()
        lines = cls.parse_out_comments(lines)
        for line in lines:
            line = line.strip()
            try:
                IPv4Address(line)
                addr_filter.add(line)
            except AddressValueError:
                try:
                    net = IPv4Network(line)
                    for ip in net:
                        addr_filter.add(str(ip))
                except AddressValueError:
                    continue
        return addr_filter

    def parse_file(self, path_to_file: Path) -> dict:
        """
        Get all IPs from input file and remove those, which do not pass the filter.
        :param path_to_file: path to input file
        :return: parsed IPs, which passed the input filter
        """
        all_ips = self._get_all_ips_from_traffic_file(path_to_file)
        final_records = {}
        for ip in all_ips:
            if self.filter_accepted_addresses is not None:
                if ip in self.filter_accepted_addresses:
                    new_record = IPv4Addr().parse(ip)
                    final_records.update(new_record)
            else:
                new_record = IPv4Addr().parse(ip)
                final_records.update(new_record)
        return final_records

    @abstractmethod
    def _get_all_ips_from_traffic_file(self, path_to_file) -> set:
        """
        This method parses all IPs, which occur in traffic file.
        :param path_to_file: path to traffic file
        :return: set of all parsed IPs
        """
        pass


class CSVTrafficParser(TrafficInputParser):
    """
    Parser used for parsing CSV traffic capture of basic netflow data.
    """
    def _get_all_ips_from_traffic_file(self, path_to_file) -> set:
        all_ips = set()
        with path_to_file.open() as input_f:
            lines = [line for line in csv.reader(input_f)]
            src_ip_index = lines[0].index("ipaddr SRC_IP")
            dst_ip_index = lines[0].index("ipaddr DST_IP")
            # in every row find IP address
            for record in lines[1:]:
                all_ips.add(record[src_ip_index])
                all_ips.add(record[dst_ip_index])
        return all_ips


class PCAPTrafficParser(TrafficInputParser):
    """
    Parser used for parsing captured traffic in .pcap file.
    """
    def _get_all_ips_from_traffic_file(self, path_to_file: Path) -> set:
        ips = set()
        packet_list = rdpcap(str(path_to_file))
        for packet in packet_list:
            if IP in packet:
                ips.add(packet[IP].src)
                ips.add(packet[IP].dst)
        return ips


class IdentifierListParser(InputParser):
    """
    Parser used for parsing list of input identifiers (ips, networks, hostnames)
    """

    @classmethod
    def _parse_file_contents(cls, contents: list, src_name: str) -> dict:
        """
        Parses out all input identifiers from loaded contents of file.
        :param contents: loaded lines of file
        :param src_name: name of source file
        :return: base structure for enrichment processing
        """
        clean_contents = cls.parse_out_comments(contents)
        records = {}
        for identifier in clean_contents:
            parser_chain = IPv4Addr(IPv4Net(Hostname()))
            new_records = parser_chain.parse(identifier)
            if new_records:
                records.update(new_records)
            else:
                cls.logger.warning(f"Identifier {identifier} in input file {src_name} is in wrong format! Must"
                                   f" be either IP address, IP network in CIDR notation or hostname!")
        return records

    def parse_file(self, path_to_file: Path) -> dict:
        """
        Load input file and process all records in it, by gradually checking, if identifier is IP address, IP network or
        hostname and create record structure from it.
        :param path_to_file: path to currently processed file
        :return: base structure for enrichment processing
        """
        with path_to_file.open() as input_f:
            contents = input_f.readlines()

        return {
            path_to_file.name: self._parse_file_contents(contents, path_to_file.name)
        }
